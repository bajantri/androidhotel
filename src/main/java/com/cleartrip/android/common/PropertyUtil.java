package com.cleartrip.android.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PropertyUtil {
	
	private static final String LOCAL_DATA_PROPERTIES_FILE_NAME = "localData.properties";
	private static final String AIR_DATA_PROPERTIES_FILE_NAME = "airData.properties";
	private static final String HOTEL_DATA_PROPERTIES_FILE_NAME = "hotelData.properties";
	private static final String TRAIN_DATA_PROPERTIES_FILE_NAME = "trainsData.properties";
	private static final String ACCOUNTS_DATA_PROPERTIES_FILE_NAME = "accountsData.properties";
	private static final String COMMON_DATA_PROPERTIES_FILE_NAME = "common.properties";
	
	private static Map<String, Properties> fileToPropertiesMap = new HashMap<>();
	
	private static Properties loadProperties(String propertyFileName) throws IOException {
		String propertyFileBaseDir = System.getProperty("user.dir") + File.separator + "resources" + File.separator;
		String propertyFilePath = propertyFileBaseDir + propertyFileName;

		Properties properties = new Properties();
		try (FileInputStream file = new FileInputStream(propertyFilePath)) {
			properties.load(file);
		}

		return properties;
	}
	
	private static Properties getProperties(String propertyFileName) throws IOException {
		Properties properties = fileToPropertiesMap.get(propertyFileName);
		if (properties == null) {
			properties = loadProperties(propertyFileName);
			fileToPropertiesMap.put(propertyFileName, properties);
		}

		return properties;
	}

	public static Properties getLocalDataProperties() throws IOException {
		return getProperties(LOCAL_DATA_PROPERTIES_FILE_NAME);
	}

	private static Properties getAirDataProperties() throws IOException {
		return getProperties(AIR_DATA_PROPERTIES_FILE_NAME);
	}

	public static Properties getHotelDataProperties() throws IOException {
		return getProperties(HOTEL_DATA_PROPERTIES_FILE_NAME);
	}
	
	public static Properties getTrainDataProperties() throws IOException {
		return getProperties(TRAIN_DATA_PROPERTIES_FILE_NAME);
	}
	
	public static Properties getAccountsDataProperties() throws IOException {
		return getProperties(ACCOUNTS_DATA_PROPERTIES_FILE_NAME);
	}
	
	public static Properties getCommonDataProperties() throws IOException {
		return getProperties(COMMON_DATA_PROPERTIES_FILE_NAME);
	}
	
	public static String getCommonData(String key) throws IOException {
		return getCommonDataProperties().getProperty(key);
	}
	
	public static String getLocalData(String key) throws IOException {
		return getLocalDataProperties().getProperty(key);
	}

	public static String getAirData(String key) throws IOException {
		return getAirDataProperties().getProperty(key);
	}

	public static String getHotelData(String key) throws IOException {
		return getHotelDataProperties().getProperty(key);
	}
	
	public static String getTrainData(String key) throws IOException {
		return getTrainDataProperties().getProperty(key);
	}
	
	public static String getAccountsData(String key) throws IOException {
		return getAccountsDataProperties().getProperty(key);
	}
}
