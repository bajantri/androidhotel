package com.cleartrip.android.common;

import static org.testng.Assert.assertTrue;

import java.io.BufferedReader;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.appium.java_client.AppiumDriver;

public class Base {

	private AppiumDriver<WebElement> driver;

	public AppiumDriver<WebElement> getDriver() {
		return driver;
	}

	private ObjectMapper mapper = new ObjectMapper();
	//public String androidOs = "6.0.1";
	
	
	
	@BeforeTest
	@Parameters({ "Device", "AndroidOs", "port" })
	public void setUp(@Optional String device, @Optional String androidOs, @Optional String port)	throws IOException, InterruptedException {
		File file = new File(".");
		File appDir = new File(file.getCanonicalPath());
		File app = new File(appDir, "Cleartrip.apk");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("automationName", "Appium");
		capabilities.setCapability("platformName", "android");
		capabilities.setCapability("appPackage", "com.cleartrip.android");
		capabilities.setCapability("appWaitPackage", "com.cleartrip.android");
		capabilities.setCapability("appActivity", "com.cleartrip.android.activity.common.SplashActivity");
		capabilities.setCapability("appWaitActivity",
				"com.cleartrip.android.activity.common.SplashActivity,com.cleartrip.android.common.activities.CleartripHomeActivity");
		capabilities.setCapability("fullReset", true);
		capabilities.setCapability("deviceName", "AndroidAppium");
		
			if (areWeRunningOnGrid()) {
			
			if (device.equals("8f4eb70024bbddc")) {
				capabilities.setCapability("platformVersion", androidOs);
				//capabilities.setCapability("app", "C:\\Users\\Automation\\Go Agent\\pipelines\\New_Android\\CleartripAPK\\cleartrip-release.apk");
				capabilities.setCapability("app", System.getProperty("user.dir") + "/Cleartrip.apk");
				driver = new AppiumDriver(new URL("http://127.0.0.1:" + port + "/wd/hub"), capabilities);
		    } else if (device.equals("520377cb4cbcb33d")) {
		    	capabilities.setCapability("platformVersion", androidOs);
				capabilities.setCapability("app", System.getProperty("user.dir") + "/Cleartrip.apk");
				driver = new AppiumDriver(new URL("http://127.0.0.1:" + port + "/wd/hub"), capabilities);
		    } else {
		    	capabilities.setCapability("platformVersion", "6.0.1");
				capabilities.setCapability("app", app.getAbsolutePath());
				driver = new AppiumDriver(new URL("http://127.0.0.1:" +4723+ "/wd/hub"), capabilities);
			}
			
			
		}

		else {
			capabilities.setCapability("deviceName", "AndroidAppium");
			capabilities.setCapability("platformVersion", getDeviceInfo().get("AndroidVersion"));
			//capabilities.setCapability("app","C:\\Users\\Automation\\Go Agent\\pipelines\\New_Android\\CleartripAPK\\cleartrip-release.apk");
			capabilities.setCapability("app", System.getProperty("user.dir") + "/Cleartrip.apk");
			System.out.println("user dr path "+System.getProperty("user.dir"));
			driver = new AppiumDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
		}

	}
	
	
	public boolean areWeRunningOnGrid() throws IOException {
		boolean isRunningOnGrid = false;
		if (CommonUtil.getCommonData("runongrid").equalsIgnoreCase("true")) {
			isRunningOnGrid = true;
		} else {
			System.out.println(
					"Please make the property run OnGrid in common.proprties file to true... Now the property is false");
		}
		return isRunningOnGrid;
	}

	
	@AfterTest(alwaysRun=true)
	public void appClose() {
		try {
			driver.quit();
		} catch (Exception e) {
			System.out.println("Session Not Found");
		}
	}
	
	
	/*@BeforeMethod
	public void setUp() throws IOException {

		File file = new File(".");
		File appDir = new File(file.getCanonicalPath());
		File app = new File(appDir, "Cleartrip.apk");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("automationName", "Appium");
		capabilities.setCapability("platformName", "android");
		capabilities.setCapability("appPackage", "com.cleartrip.android");
		capabilities.setCapability("appWaitPackage", "com.cleartrip.android");
		capabilities.setCapability("appActivity", "com.cleartrip.android.activity.common.SplashActivity");
		capabilities.setCapability("appWaitActivity",
				"com.cleartrip.android.activity.common.SplashActivity,com.cleartrip.android.common.activities.CleartripHomeActivity");
		capabilities.setCapability("fullReset", true);
		capabilities.setCapability("deviceName", "AndroidAppium");
		capabilities.setCapability("platformVersion", "6.0.1");
		capabilities.setCapability("app", System.getProperty("user.dir") + "/Cleartrip.apk");
		driver = new AppiumDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
	}


	@BeforeSuite
	public void installAndSetUp() throws IOException, InterruptedException {
		
		CommonPageObjects commonPageObjects = new CommonPageObjects(driver);
		//commonPageObjects.setWifiConnection(driver, true, Main.getEnv());
		
		File file = new File(".");
		File appDir = new File(file.getCanonicalPath());
		File app = new File(appDir, "Cleartrip.apk");
		
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("automationName", "Appium");
		capabilities.setCapability("platformName", "android");
		capabilities.setCapability("appPackage", "com.cleartrip.android");
		capabilities.setCapability("appWaitPackage", "com.cleartrip.android");
		capabilities.setCapability("appActivity", "com.cleartrip.android.activity.common.SplashActivity");
		capabilities.setCapability("appWaitActivity",
				"com.cleartrip.android.activity.common.SplashActivity,com.cleartrip.android.common.activities.CleartripHomeActivity");
		//capabilities.setCapability("fullReset", true);
		capabilities.setCapability("noResetValue",false);
		capabilities.setCapability("app", app.getAbsolutePath());

		CommonUtil util = new CommonUtil();
		if (areWeRunningOnGrid()) {
			HashMap<String, HashMap<String, String>> deviceMap = new HashMap<>();
			deviceMap = deviceParams(util.getCommonData("devicejson"));
			launchingRemoteWebDriver(deviceMap, capabilities);
		} else {
			capabilities.setCapability("deviceName", "AndroidAppium");
			capabilities.setCapability("platformVersion", getDeviceInfo().get("AndroidVersion"));
			driver = new AppiumDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
		}
	}

	@BeforeMethod
	public void setUp1() throws IOException {

		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("automationName", "Appium");
		capabilities.setCapability("platformName", "android");
		capabilities.setCapability("appPackage", "com.cleartrip.android");
		capabilities.setCapability("appWaitPackage", "com.cleartrip.android");
		capabilities.setCapability("appActivity", "com.cleartrip.android.activity.common.SplashActivity");
		capabilities.setCapability("appWaitActivity",
				"com.cleartrip.android.activity.common.SplashActivity,com.cleartrip.android.common.activities.CleartripHomeActivity");
		capabilities.setCapability("fullReset", true);
		capabilities.setCapability("noResetValue",true);

		
		if (areWeRunningOnGrid()) {
			HashMap<String, HashMap<String, String>> deviceMap = new HashMap<>();
			deviceMap = deviceParams(CommonUtil.getCommonData("devicejson"));
			launchingRemoteWebDriver(deviceMap, capabilities);
		} else {
			capabilities.setCapability("deviceName", "AndroidAppium");
			capabilities.setCapability("platformVersion", getDeviceInfo().get("AndroidVersion"));
			capabilities.setCapability("app", System.getProperty("user.dir") + "/Cleartrip.apk");
			driver = new AppiumDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
		}

	}

	public boolean areWeRunningOnGrid() throws IOException {
		boolean isRunningOnGrid = false;
		if (CommonUtil.getCommonData("runongrid").equalsIgnoreCase("true")) {
			isRunningOnGrid = true;
		} else {
			System.out.println(
					"Please make the property runOnGrid in common.proprties file to true... Now the property is false");
		}
		return isRunningOnGrid;
	}

	public HashMap<String, HashMap<String, String>> deviceParams(String json)
			throws JsonParseException, JsonMappingException, IOException {

		DeviceManager deviceManager = mapper.readValue(json, DeviceManager.class);
		HashMap<String, HashMap<String, String>> deviceMap = new HashMap<String, HashMap<String, String>>();
		int index = 1;
		for (Device d : deviceManager.getDevices()) {
			HashMap<String, String> devicesAttribute = new HashMap<String, String>();
			devicesAttribute.put("uuid", d.getDeviceId());
			devicesAttribute.put("osVersion", d.getOsVersion());
			devicesAttribute.put("port", d.getPort());
			deviceMap.put("device"+index, devicesAttribute);
			index++;
			
		}
		System.out.println(deviceMap);

		return deviceMap;

	}

	public void launchingRemoteWebDriver(HashMap<String, HashMap<String, String>> deviceMap,
			DesiredCapabilities capabilities) throws MalformedURLException {

		for (Map.Entry<String, HashMap<String, String>> entry : deviceMap.entrySet()) {

			String deviceId = entry.getValue().get("uuid");
			int portNumber = Integer.parseInt(entry.getValue().get("port"));
			String osVersion = entry.getValue().get("osVersion");

			capabilities.setCapability("deviceName", deviceId);
			capabilities.setCapability("platformVersion", osVersion);
			driver = new AppiumDriver(new URL("http://127.0.0.1:" + portNumber + "/wd/hub"), capabilities);

		}
	}

	
	
	@AfterMethod(alwaysRun = true)
	public void takeScreenshot(ITestResult _result) throws Exception {
	    try {
			driver.quit();
		} catch (Exception e) {
			System.out.println("Session Not Found");
		}

	}
	*/
	public Map<String, String> getDeviceInfo() throws IOException {
		// adb shell cat /system/build.prop | find 'ro.build.version'
		// adb shell cat /system/build.prop | grep 'ro.build.version'
		Map<String, String> deviceInfoMap = new HashMap<String, String>();
		Runtime rt = Runtime.getRuntime();
		Process p = rt.exec("adb shell cat /system/build.prop | grep 'ro.build.version'");
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line = "";
		while ((line = reader.readLine()) != null) {

			if (line.toLowerCase().contains("sdk")) {
				deviceInfoMap.put("SDKVersion", line.split("=")[1].trim());
			} else if (line.toLowerCase().contains("release")) {
				deviceInfoMap.put("AndroidVersion", line.split("=")[1].trim());
			}
		}
		return deviceInfoMap;
	}
	
	/*@BeforeTest
	@Parameters({ "Device", "AndroidOs", "port" })
	public void checkDeviceStatus(@Optional String device, @Optional String androidOs, @Optional String port)
			throws Exception {

		try {
			boolean flag = false;
			int i = 0;
			do {
				flag = false;
				System.out.println("=================================");
				System.out.println("Checking status of device " + device);
				System.out.println("=================================");
				Runtime rt = Runtime.getRuntime();
				Process p1 = rt.exec("adb -s " + device + " shell dumpsys cpuinfo");
				BufferedReader reader1 = new BufferedReader(new InputStreamReader(p1.getInputStream()));
				System.out.println(reader1.toString());
				String line1 = "";
				l1: while ((line1 = reader1.readLine()) != null) {
					if (line1.toString().toLowerCase().contains("com.cleartrip.android".toLowerCase())) {
						flag = true;
						break l1;
					}
				}
				if (flag) {
					Thread.sleep(60000);
				}
				i++;
			} while (flag && i < 16);
		} catch (Exception e) {
			System.out.println("Error in checking device status");
		}
	}*/
	
	
	
	
	//@BeforeMethod
	
	
	




}
