package com.cleartrip.android.common;

import org.apache.commons.logging.impl.Log4JLogger;
import org.apache.tools.ant.taskdefs.WaitFor.Unit;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.cleartrip.android.objects.hotel.PaymentPageObjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.HidesKeyboard;
import io.appium.java_client.TouchAction;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.sql.Driver;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Year;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class CommonUtil extends PropertyUtil {

	public void safeType(AppiumDriver driver, By by, String text) throws InterruptedException {
		boolean elementPresentFlag = false;
		if (elementPresent(driver, by, 5)) {
			driver.findElement(by).sendKeys(text);
		} else {
			assertTrue(elementPresentFlag, "Element : " + String.valueOf(by) + " not present ");
		}
	}

	public boolean elementVisible(AppiumDriver driver, By by, int time) throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		boolean elementNotVisibleFlag = false;
		for (int i = 0; i < time; i++) {
			try {
				if (driver.findElement(by).isDisplayed()) {
					System.out.println("Element displayed" + by);
					elementNotVisibleFlag = true;
					break;
				}

			} catch (Exception e) {
				System.out.println("Element : " + by + " not Visible");
			}
			Thread.sleep(1000);
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return elementNotVisibleFlag;
	}

	public List<WebElement> s(AppiumDriver driver, By by) {
		boolean elementPresentFlag = false;
		if (elementPresentFlag = elementPresent(driver, by, 2)) {
			return driver.findElements(by);
		}
		return null;
	}

	public boolean elementPresent(AppiumDriver driver, By by, int time) {

		// WebDriverWait wait = new WebDriverWait(driver, time);

		driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
		boolean elementPresentFlag = false;
		try {

			driver.findElement(by);
			// wait.until(ExpectedConditions.presenceOfElementLocated(by));
			elementPresentFlag = true;

		} catch (NoSuchElementException e) {
			System.out.println("not found");

		}

		return elementPresentFlag;
	}

	public void safeClick(AppiumDriver driver, By by) throws Exception {
		boolean elementPresentFlag = false;
		if (elementPresentFlag = elementVisible(driver, by, 8)) {
			driver.findElement(by).click();
		} else {
			assertTrue(elementPresentFlag, "Elements : " + String.valueOf(by) + " not present ");
		}
	}

	public boolean waitForElement(AppiumDriver driver, By by) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		boolean isElementVisible = false;
		if (isElementVisible == false) {
			wait.until(ExpectedConditions.presenceOfElementLocated(by));
			isElementVisible = true;
		}
		return isElementVisible;
	}

	public boolean waitForElementAsString(AppiumDriver<WebElement> driver, String product) {
		boolean isElementPresent = false;

		WebDriverWait wait = new WebDriverWait(driver, 20);

		if (!product.isEmpty() && product != null) {
			wait.until(ExpectedConditions
					.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='" + product + "'" + "]")));
			isElementPresent = true;
		} else {
			System.out.println("element not found : ");

		}

		return isElementPresent;

	}

	public WebElement swipeDown(AppiumDriver driver, String elementName) throws InterruptedException {

		Dimension size = driver.manage().window().getSize();
		boolean isElementFound = false;
		int height = size.height;
		int width = size.width;
		int startY = (int) (height * 0.70);
		int endY = (int) (height * 0.40);
		int startX = (int) (width / 2);
		int endX = (int) (width / 2);
		WebElement elementPresent = null;
		TouchAction action = new TouchAction(driver);
		while (isElementFound == false) {
			action.press(startX, startY).waitAction(Duration.ofMillis(650)).moveTo(endX, endY).release().perform();
			try {
				if (driver.findElement(By.xpath("//android.widget.TextView[@text='" + elementName + "'" + "]"))
						.isDisplayed()) {
					elementPresent = driver
							.findElement(By.xpath("//android.widget.TextView[@text='" + elementName + "'" + "]"));
					isElementFound = true;
					break;
				}
			} catch (Exception e) {
				System.out.println("Element not found");
			}
		}

		return elementPresent;
	}

	public WebElement swipeDownById(AppiumDriver<WebElement> driver, By by) throws InterruptedException {

		Dimension size = driver.manage().window().getSize();
		boolean isElementFound = false;

		int height = size.height;
		int width = size.width;
		int startY = (int) (height * 0.80);
		int endY = (int) (height * 0.60);
		int startX = (int) (width / 2);
		int endX = (int) (width / 2);
		WebElement elementPresent = null;
		TouchAction action = new TouchAction(driver);
		while (isElementFound == false) {
			action.press(startX, startY).waitAction(Duration.ofMillis(500)).moveTo(endX, endY).release().perform();
			try {
				if (driver.findElement(by).isDisplayed()) {
					elementPresent = driver.findElement(by);
					isElementFound = true;
					break;
				}
			} catch (Exception e) {
				System.out.println("Element not found");
			}
		}

		return elementPresent;
	}

	public String dateSelection(AppiumDriver driver) {

		LocalDate currentDate = LocalDate.now();
		String monthName = currentDate.getMonth().name();
		int year = currentDate.getYear();
		String month[] = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };

		for (int i = 0; i < month.length; i++) {
			if (monthName.equalsIgnoreCase("April")) {
				monthName = "February";
				break;
			} else if (monthName.equalsIgnoreCase("April")) {
				monthName = "January";
				break;
			} else if (monthName.equalsIgnoreCase(month[i])) {
				monthName = month[i + 2];
				break;
			}

		}
		System.out.println(monthName);
		return monthName + " " + year;
	}

	public HashMap<String, String> registerParams() throws IOException {
		HashMap<String, String> register = new HashMap<>();

		register.put("title", getAccountsData("title"));
		register.put("firstName", getAccountsData("firstname"));
		register.put("lastName", getAccountsData("lastname"));
		register.put("emailAddress", randomEmailGeneration());
		register.put("mobileNumber", getAccountsData("mobilenumber"));
		register.put("password", getAccountsData("password"));

		return register;
	}

	public String randomEmailGeneration() {
		double random = Math.round(Math.random() * 123);
		String emailAddress = "automationTest" + random + "@gmail.com";
		return emailAddress;

	}

	public void scrollDown(AppiumDriver<WebElement> driver, By element) {
		Dimension dimensions = driver.manage().window().getSize();

		int pressX = driver.manage().window().getSize().width / 2;
		System.out.println(pressX);

		int bottomY = driver.manage().window().getSize().height * 4 / 5;
		System.out.println(bottomY);

		int topY = driver.manage().window().getSize().height / 8;

		for (int i = 0; i < dimensions.getHeight(); i++) {
			System.out.println(dimensions.getHeight());

			TouchAction action = new TouchAction(driver);
			action.press(pressX, bottomY).waitAction(Duration.ofMillis(500)).moveTo(pressX, topY).release().perform();

			try {
				if (driver.findElement(element).isDisplayed())
					break;
			} catch (Exception e) {
				System.out.println("element not visible");
			}

		}

	}

	public void expiryScroll(AppiumDriver driver) throws Exception {
		int expiryMonth = Integer.parseInt("05");
		int expiryYear = Integer.parseInt("2020");
		final String pattern = "dd-MM-yyyy";
		String dateInString = new SimpleDateFormat(pattern).format(new Date());
		// System.out.println(dateInString);
		int currentMonth = Integer.parseInt(dateInString.split("-")[1]);
		int currentYear = Integer.parseInt(dateInString.split("-")[2]);
		if (currentMonth > expiryMonth) {
			while (currentMonth != expiryMonth) {
				// System.out.println("curMon " + expiryMonth);
				shortSwipeDown(driver, 0.8);
				Thread.sleep(2000);
				currentMonth--;
			}
		} else {
			while (currentMonth != expiryMonth) {
				// System.out.println("curMon:" + currentMonth);
				shortSwipe1(driver, 0.8);
				Thread.sleep(2000);
				currentMonth++;

			}
		}
		// this.expiryYear.click();
		while (currentYear != expiryYear) {
			// System.out.println("currYer" + currentYear);
			shortSwipe1(driver, 1.3);
			Thread.sleep(2000);
			currentYear++;
		}
	}

	public void shortSwipeUp(AppiumDriver driver, double diffX) {
		Dimension windowSize = driver.manage().window().getSize();
		int height = windowSize.height;
		int width = windowSize.width;
		int startY = (int) (height * 0.60);
		int endY = (int) (height * 0.50);
		int startX = (int) (width / 2);
		TouchAction action = new TouchAction(driver);
		try {
			int j = (int) (startX * diffX);

			action.press(j, endY).waitAction(Duration.ofMillis(500)).moveTo(j, startY).release().perform();

		} catch (Exception e) {
			System.out.println("Unable to do short swipe");
		}
	}

	public void shortSwipe(AppiumDriver driver, int diffX) {
		Dimension windowSize = driver.manage().window().getSize();
		int height = windowSize.height;
		int width = windowSize.width;
		int startY = (int) (height * 0.60);
		int endY = (int) (height * 0.50);
		int startX = (int) (width / 2);
		TouchAction action = new TouchAction(driver);
		try {

			action.press(startX - diffX, startY).waitAction(Duration.ofMillis(500)).moveTo(startX - diffX, endY)
					.release().perform();

		} catch (Exception e) {
			System.out.println("Unable to do short swipe");
		}
	}

	public void shortSwipe1(AppiumDriver driver, double diffX) {
		Dimension windowSize = driver.manage().window().getSize();
		int height = windowSize.height;
		int width = windowSize.width;
		int startY = (int) (height * 0.60);
		int endY = (int) (height * 0.53);
		int startX = (int) (width / 2);
		TouchAction action = new TouchAction(driver);
		try {
			int j = (int) (startX * diffX);

			action.press(j, startY).waitAction(Duration.ofMillis(500)).moveTo(j, endY).release().perform();
		} catch (Exception e) {
			System.out.println("Unable to do short swipe");
		}

	}

	public void shortSwipeDown(AppiumDriver driver, double diffX) {
		Dimension windowSize = driver.manage().window().getSize();
		int height = windowSize.height;
		int width = windowSize.width;
		int startY = (int) (height * 0.60);
		int endY = (int) (height * 0.50);
		int startX = (int) (width / 2);
		TouchAction action = new TouchAction(driver);

		try {
			int j = (int) (startX * diffX);

			action.press(j, endY).waitAction(Duration.ofMillis(500)).moveTo(j, startY).release().perform();
		} catch (Exception e) {
			System.out.println("Unable to do short swipe");
		}

	}

	public void makePayment(AppiumDriver driver, String paymentMode, String paymentOption, String coupon)
			throws Exception {
		if (CommonUtil.getCommonData("newdesignorold").equalsIgnoreCase("oldflow")) {

			CommonPageObjects cp = new CommonPageObjects(driver);
			if (paymentMode.equalsIgnoreCase("creditcard")) {
				creditCardPayment(driver, paymentOption);
			} else if (paymentMode.equalsIgnoreCase("storedcard")) {
				storedCardPayment(driver, paymentOption);
			} else if (paymentMode.equalsIgnoreCase("netbanking")) {
				netBankingPayment(driver, paymentOption);
			} else if (paymentMode.equalsIgnoreCase("thirdpartywallet")) {
				thirdPartyWalletPayment(driver, paymentOption);
			}
			swipeDownById(driver, By.id("com.cleartrip.android:id/btnSubmitPayment"));
			if (!coupon.isEmpty() && coupon != null) {
				couponCode(driver, coupon);
			} else {
				System.out.println("Booking without coupon");
			}
			Thread.sleep(2000);
			swipeDownById(driver, By.id("com.cleartrip.android:id/btnSubmitPayment"));
			cp.getPayButton().click();

		}

		// New payment page credit card booking
		else if (CommonUtil.getCommonData("newdesignorold").equalsIgnoreCase("newflow")) {
			CommonPageObjects cp = new CommonPageObjects(driver);
			cp.getNewCreditCardFieldID().sendKeys(getCommonData("mastercardnumber"));
			driver.hideKeyboard();
			cp.getExpiryMonthAndYearNewId().click();
			Thread.sleep(2000);
			expiryScroll(driver);
			cp.getExpiryDialogueOkButton().click();
			cp.getCvvInNewDesignPage().sendKeys(getCommonData("mastercardcvv"));
			driver.hideKeyboard();
			cp.getPayButtonNewId().click();
			System.out.println("payment button click done");

		}
	}

	public void creditCardPayment(AppiumDriver driver, String paymentOption) throws Exception {
		CommonPageObjects cp = new CommonPageObjects(driver);
		// cp.getCardAsPaymentOption().click();
		if (paymentOption.equalsIgnoreCase("mastercard")) {
			cp.getCreditCardNumber().sendKeys(getCommonData("mastercardnumber"));
			Thread.sleep(2000);
			driver.hideKeyboard();
			cp.getExpiryMonthAndYear().click();
			expiryScroll(driver);
			cp.getExpiryDialogueOkButton().click();
			Thread.sleep(2000);
			swipeDownById(driver, By.id("com.cleartrip.android:id/btnSubmitPayment"));
			cp.getcvv().sendKeys(getCommonData("mastercardcvv"));
			driver.hideKeyboard();
		} else if (paymentOption.equalsIgnoreCase("amex")) {
			cp.getCreditCardNumber().sendKeys(getCommonData("amexcardnumber"));
			cp.getExpiryMonthAndYear().click();
			expiryScroll(driver);
			cp.getExpiryDialogueOkButton();
			waitForElement(driver, By.id("com.cleartrip.android:id/credit_cvv"));
			cp.getcvv().sendKeys(getCommonData("amexcardcvv"));
		}

	}

	public void storedCardPayment(AppiumDriver driver, String paymentOption) throws IOException, InterruptedException {
		CommonPageObjects cp = new CommonPageObjects(driver);
		cp.getCardAsPaymentOption().click();
		if (paymentOption.equalsIgnoreCase("mastercard")) {
			driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'5123')]")).click();
			cp.getcvv().sendKeys(getCommonData("mastercardcvv"));
		} else if (paymentOption.equalsIgnoreCase("amex")) {
			driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'3456')]")).click();
			cp.getcvv().sendKeys(getCommonData("amexcardcvv"));
		}

	}

	public void netBankingPayment(AppiumDriver driver, String paymentOption) throws InterruptedException, IOException {
		CommonPageObjects cp = new CommonPageObjects(driver);
		cp.getNetBankingAsPaymentOption().click();
		cp.getNetBankingList().click();
		swipeDown(driver, getCommonData("netbankingbank"));
		cp.getNetBankingBank().click();
		swipeDownById(driver, By.id("com.cleartrip.android:id/btnSubmitPayment"));

	}

	public void thirdPartyWalletPayment(AppiumDriver driver, String paymentOption) throws InterruptedException {
		CommonPageObjects cp = new CommonPageObjects(driver);
		cp.getThirdPartyWalletAsPaymentOption().click();
		cp.selectWallet(driver, paymentOption);
		swipeDownById(driver, By.id("com.cleartrip.android:id/btnSubmitPayment"));

	}

	public void contextSwitch(AppiumDriver driver) throws InterruptedException, IOException {

		if (contextSwitchForPayment()) {
			Thread.sleep(6000);

			driver.findElement(By.xpath("//android.widget.Button[@text='Submit']")).click();
		} else {
		}

	}

	public boolean contextSwitchForPayment() throws IOException {
		boolean iscontextSwitchForPayment = false;
		if (getHotelData("contextSwitch").equalsIgnoreCase("true")) {
			iscontextSwitchForPayment = true;
		}
		return iscontextSwitchForPayment;

	}
	/*
	 * Set<String> contextNames = driver.getContextHandles(); for (String
	 * contextName : contextNames) { System.out.println(contextNames); //prints out
	 * something like NATIVE_APP \n WEBVIEW_1 } driver.context((String)
	 * contextNames.toArray()[1]); // set context to WEBVIEW_1
	 * 
	 * //do some web testing
	 * driver.findElement(By.xpath("//android.widget.Button[@text='Submit']")).click
	 * ();
	 * 
	 * driver.context("NATIVE_APP");
	 * 
	 * // do more native testing if we want
	 * 
	 * driver.quit();
	 */

	// driver = new AppiumDriver(new URL("http://127.0.0.1:4723/wd/hub"),
	// capabilities);

	/*
	 * Set<String> contextNames = driver.getContextHandles(); for (String
	 * contextName : contextNames) { System.out.println(contextNames); //prints out
	 * something like NATIVE_APP \n WEBVIEW_1 } driver.context((String)
	 * contextNames.toArray()[1]); // set context to WEBVIEW_1
	 * 
	 * //do some web testing
	 * driver.findElement(By.xpath("//android.widget.Button[@text='Submit']")).click
	 * ();
	 * 
	 * driver.context("NATIVE_APP");
	 */

	public void switchToNativeApp(AppiumDriver driver) {
		Set<String> allWindows = driver.getContextHandles();
		for (String window : allWindows) {
			System.out.println(window);
			if (window.contains("NATIVE_APP")) {
				driver.context(window);
				// driver.switchTo().window(window);
				driver.findElement(By.xpath("//android.widget.Button[@text='Submit']")).click();

			}
		}

	}

	public void couponCode(AppiumDriver driver, String coupon) {
		PaymentPageObjects pay = new PaymentPageObjects(driver);
		pay.getCouponcodeField().sendKeys(coupon);
		driver.hideKeyboard();
		waitForElement(driver, By.id("com.cleartrip.android:id/check_savings"));
		pay.getCheckSavingsButton().click();
		System.out.println("coupon applied  successfully");
		// String couponMessage =
		// driver.findElement(By.id("com.cleartrip.android:id/cpn_message")).getText();
		// String couponMessage =
		// driver.findElement(By.id("com.cleartrip.android:id/couponCode")).getText();
		/*
		 * boolean coupon1 =
		 * driver.findElement(By.id("com.cleartrip.android:id/cpn_success")) != null;
		 * 
		 * System.out.println("message after applying coupon is :::::::::" + coupon1);
		 * if (coupon1 = true) { System.out.println("coupon applied  successfully"); }
		 * else { System.out.println("coupon code failed "); }
		 */
	}

	public void netBankRetry(AppiumDriver driver) throws InterruptedException, IOException {
		if (CommonUtil.getCommonData("newdesignorold").equalsIgnoreCase("oldflow")) {
			PaymentPageObjects pm = new PaymentPageObjects(driver);
			pm.getNetBankingLink().click();
			waitForElement(driver, By.id("com.cleartrip.android:id/lytNetBanking"));
			pm.getSelectBankDropDown().click();
			waitForElement(driver, By.xpath("//android.widget.TextView[@text='State Bank of India']"));
			pm.getSelectBank().click();
			swipeDownById(driver, By.id("com.cleartrip.android:id/btnSubmitPayment"));
			pm.getPayButton().click();
			CommonUtil util = new CommonUtil();
			util.waitForElement(driver, By.xpath("//android.view.View[@text='Click here']"));
			swipeDownById(driver, By.xpath("//android.view.View[@text='Click here']"));
			driver.findElement(By.xpath("//android.view.View[@text='Click here']")).click();

			util.waitForElement(driver, By.id("android:id/alertTitle"));
			driver.findElement(By.id("android:id/button1")).click();
			util.waitForElement(driver, By.id("com.cleartrip.android:id/btnCreditNormal"));
			driver.findElement(By.id("com.cleartrip.android:id/btnCreditNormal")).click();

		} else if (CommonUtil.getCommonData("newdesignorold").equalsIgnoreCase("newflow")) {
			PaymentPageObjects pm = new PaymentPageObjects(driver);
			CommonPageObjects cp = new CommonPageObjects(driver);
			pm.getNetBankingDropDownButton().click();
			System.out.println("list of banks " + pm.getSelectBankFromList().size());
			pm.getSelectBankFromList().get(3).click();
			cp.getPayButtonNewId().click();

			waitForElement(driver, By.xpath("//android.view.View[@text='Click here']"));
			swipeDownById(driver, By.xpath("//android.view.View[@text='Click here']"));
			driver.findElement(By.xpath("//android.view.View[@text='Click here']")).click();
			WebDriverWait wait = new WebDriverWait(driver, 30);

			Thread.sleep(3000);
			if (driver.findElementByXPath("//android.widget.Image[@text='Need assistance?']").isDisplayed()) {
				driver.navigate().back();
				waitForElement(driver, By.id("android:id/button1"));
				driver.findElement(By.id("android:id/button1")).click();
				cp.getPayButtonNewId().click();
				waitForElement(driver, By.xpath("//android.view.View[@text='Click here']"));
				swipeDownById(driver, By.xpath("//android.view.View[@text='Click here']"));
				driver.findElement(By.xpath("//android.view.View[@text='Click here']")).click();

			} else {
				waitForElement(driver, By.id("android:id/alertTitle"));
				// Assert.assertEquals(driver.findElement(By.id("")).getText(), "select
				// payment");
				driver.findElement(By.id("android:id/button1")).click();
				waitForElement(driver, By.xpath("//android.widget.TextView[@text='Credit/Debit Card"));
				driver.findElementByXPath("//android.widget.TextView[@text='Credit/Debit Card").click();

			}

		}

	}

	public boolean isPayment() {

		return true;
	}

	public void netBankRetryForPaymentRetry(AppiumDriver driver) throws InterruptedException {
		PaymentPageObjects pm = new PaymentPageObjects(driver);
		pm.getNetBankingLink().click();
		waitForElement(driver, By.id("com.cleartrip.android:id/lytNetBanking"));
		pm.getSelectBankDropDown().click();
		waitForElement(driver, By.xpath("//android.widget.TextView[@text='State Bank of India']"));
		pm.getSelectBank().click();
		swipeDownById(driver, By.id("com.cleartrip.android:id/btnSubmitPayment"));
		pm.getPayButton().click();
	}

	public void pahccCreditCardPayment(AppiumDriver driver, String paymentMode, String paymentOption, String coupon)
			throws Exception {
		CommonPageObjects cp = new CommonPageObjects(driver);
		cp.getNewCreditCardFieldID().sendKeys(getCommonData("mastercardnumber"));
		driver.hideKeyboard();
		cp.getExpiryMonthAndYearNewId().click();
		Thread.sleep(2000);
		expiryScroll(driver);
		cp.getExpiryDialogueOkButton().click();
		cp.getCvvInNewDesignPage().sendKeys(getCommonData("mastercardcvv"));
		driver.hideKeyboard();
		swipeDownById(driver, By.id("com.cleartrip.android:id/btnSubmitPayment"));
		driver.findElement(By.id("com.cleartrip.android:id/btnSubmitPayment")).click();
	}

	/*
	 * public void assertMethod(boolean a) {
	 * 
	 * Assert.assertTrue(a);
	 * 
	 * }
	 */

	public void hotelCancellation(AppiumDriver driver) throws InterruptedException {

		CommonPageObjects cp = new CommonPageObjects(driver);
		/*
		 * cp.getViewTripDetailsButton().click();
		 * if(cp.getOkayPopUpInTripDetailsPage().isDisplayed()) {
		 * cp.getOkayPopUpInTripDetailsPage().click(); } driver.navigate().back();
		 * cp.getViewTripDetailsButton().click();
		 */
		cp.getCancelButtonInTripDetailsPage().click();
		waitForElement(driver, By.xpath("//android.view.View[@text='Review cancellations & confirm']"));
		swipeDownById(driver, By.id("trip_cancel"));

		/*
		 * for(int i=0;i<3;i++) {
		 * 
		 * if (driver.findElementById("noCancel").isDisplayed()) {
		 * driver.findElementById("trip_cancel").click(); break; } else {
		 */

		for (int i = 0; i < 3; i++) {
			boolean cancelbutton = false;
			TouchAction touch = new TouchAction(driver);
			Dimension size = driver.manage().window().getSize();
			boolean isElementFound = false;
			int height = size.height;
			int width = size.width;
			int startY = (int) (height * 0.70);
			int endY = (int) (height * 0.40);
			int startX = (int) (width / 2);
			int endX = (int) (width / 2);
			WebElement elementPresent = null;
			touch.press(startX, startY).waitAction(Duration.ofMillis(650)).moveTo(endX, endY).release().perform();
			cancelbutton = waitForElement(driver, By.id("noCancel"));
			cp.getYesCancelNowButton().isDisplayed();

		}

		cp.getYesCancelNowButton().click();
		System.out.println(cp.getCancellationConfirmationPage().getText());
	}

	// swipeDownById(driver, By.id("noCancel"));

}
