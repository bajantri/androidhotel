package com.cleartrip.android.common;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;

public class CleartripLandingPageObjects extends PageObject {

	public CleartripLandingPageObjects(AppiumDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@FindBy(id = "com.android.packageinstaller:id/permission_allow_button")
	private WebElement allowPopUp;

	@FindBy(id = "com.android.packageinstaller:id/permission_deny_button")
	private WebElement denyPopUp;

	@FindBy(id = "com.android.packageinstaller:id/dialog_container")
	private WebElement allowPopUpFrame;

	public WebElement getAllowPopUpFrame() {
		return allowPopUpFrame;
	}

	@FindBy(id = "com.cleartrip.android:id/original_travel_text")
	WebElement travel;

	@FindBy(id = "com.cleartrip.android:id/original_local_text")
	WebElement local;
	
	@FindBy(id="com.android.packageinstaller:id/permission_allow_button")
	private WebElement photoAndMediaAllow;

	/*
	 * @FindBy(xpath=("//android.widget.TextView[@text='productName']")) private
	 * WebElement productIcon;
	 */

	public WebElement getPhotoAndMediaAllow() {
		return photoAndMediaAllow;
	}

	public WebElement getProductIcon(String product) {
		return driver.findElement(By.xpath("//android.widget.TextView[@text='" + product + "'" + "]"));
	}

	public void selectProduct(String product) throws InterruptedException {
		WebElement clickProductIcon = getProductIcon(product);
		clickProductIcon.click();

	}

	public WebElement getAllowPopUp() {
		return allowPopUp;
	}

	public WebElement getDenyPopUp() {
		return denyPopUp;
	}

	public WebElement getTravel() {
		return travel;
	}

	public WebElement getLocal() {
		return local;
	}

	public void clickPopUp(AppiumDriver driver, boolean allowPermission) {
		CommonUtil util = new CommonUtil();
		if (allowPermission) {
			util.waitForElement(driver, By.id("com.android.packageinstaller:id/dialog_container"));
			getAllowPopUp().click();
		} else if(allowPermission) {
			util.waitForElement(driver, By.id("com.android.packageinstaller:id/dialog_container"));
			getDenyPopUp().click();
		}
		else if(allowPermission) {
			util.waitForElement(driver, By.id("com.android.packageinstaller:id/permission_allow_button"));
			getPhotoAndMediaAllow().click();		}
		
		
			
	}

	public void travelLocalSwitch(AppiumDriver driver, By by, String product) {

		clickPopUp(driver, false);
		//switchToggle(product);

	}

	public void switchToggle(String product) {
		CommonUtil ct=new CommonUtil();
		ct.waitForElement(driver, By.id("com.cleartrip.android:id/original_travel_text"));
		/*if (product.equalsIgnoreCase("travel")) {
			getTravel().click();
		} else {
			getLocal().click();
		}*/
		getTravel().click();
	}

	public void switchProduct(AppiumDriver driver, String product) throws InterruptedException {
		CommonUtil util = new CommonUtil();
		boolean isProductPresent = util.waitForElementAsString(driver, product);
		if (isProductPresent) {
			selectProduct(product);
		} else {
			System.out.println("Element not visible :" + product);
		}

	}

}
