package com.cleartrip.android.common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.gargoylesoftware.htmlunit.javascript.host.media.webkitMediaStream;

import io.appium.java_client.AppiumDriver;

public class CommonPageObjects extends PageObject {

	public CommonPageObjects(AppiumDriver<WebElement> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@FindBy(xpath = "//android.widget.TextView[@text='Settings']")
	private WebElement settingsIcon;

	@FindBy(xpath = "//android.widget.TextView[@text='Wi-Fi']")
	private WebElement wifiLink;

	@FindBy(xpath = "//android.widget.Switch[@text='ON']")
	private WebElement wifiOnOffToggle;

	@FindBy(className = "android.widget.LinearLayout")
	private List<WebElement> wifiList;

	@FindBy(id = "com.android.settings:id/password")
	private WebElement settingsPassword;

	@FindBy(xpath = "//android.widget.Button[@text='CONNECT']")
	private WebElement connectWifiLink;

	@FindBy(xpath = "//android.widget.TextView[@text='Connected']")
	private WebElement connectedWifiLink;

	@FindBy(xpath = "//android.widget.Button[@text='FORGET']")
	private WebElement forgetWifiLink;

	@FindBy(id="com.cleartrip.android:id/newCardCTA")
	private WebElement useNewCardLinkInPaymentPage;
	
	public WebElement getUseNewCardLinkInPaymentPage() {
		return useNewCardLinkInPaymentPage;
	}

	public WebElement getForgetWifiLink() {
		return forgetWifiLink;
	}

	public WebElement getConnectedWifiLink() {
		return connectedWifiLink;
	}

	public WebElement getConnectWifiLink() {
		return connectWifiLink;
	}

	public WebElement getSettingsPassword() {
		return settingsPassword;
	}

	public WebElement getSettingsIcon() {
		return settingsIcon;
	}

	public WebElement getWifiLink() {
		return wifiLink;
	}

	public WebElement getWifiOnOffToggle() {
		return wifiOnOffToggle;
	}

	public List<WebElement> getWifiList() {
		return wifiList;
	}

	public void settingIconClick() {
		getSettingsIcon().click();
	}

	public void wifiLinkClick() {
		getWifiLink().click();
	}

	public void wifiToggleOn() {
		getWifiOnOffToggle().click();
	}

	/*
	 * public void wifiListCollect(){ List<WebElement> wifiListing = new
	 * ArrayList<>(); wifiListing = getWifiList(); }
	 */

	public boolean setWifiConnection(AppiumDriver<WebElement> driver, boolean connectToWifi, String env)
			throws InterruptedException, IOException {
		boolean isWifiConnected = false;
		CommonUtil util = new CommonUtil();
		if (env.equalsIgnoreCase("QA")) {
			env = "CT_CLR_QA2";
		} else if (env.equalsIgnoreCase("HF")) {
			env = "CT_BLR_HF";
		} else if (env.equalsIgnoreCase("STACK3")) {
			env = "CHN2R3";
		} else if (env.equalsIgnoreCase("OFFICE")) {
			env = "BLR_MOBILE";
		} else {
			System.out.println("Please specify the environment to select the wifi");
		}

		if (connectToWifi) {
			if (util.waitForElementAsString(driver, "Connected")) {
				getConnectedWifiLink().click();
				getForgetWifiLink().click();
			} else {
				System.out.println("No wifi is connected and hence, finding a suitable network to connect");
			}
			for (WebElement we : getWifiList()) {
				String wifiName = we.getText();
				if (util.swipeDown(driver, env).toString().toLowerCase().contains(wifiName)) {
					we.click();
					getSettingsPassword().clear();
					getSettingsPassword().sendKeys(CommonUtil.getCommonData(env + "wifipassword"));
					getConnectWifiLink().click();
					util.waitForElementAsString(driver, "Connected");
					isWifiConnected = true;
					break;
				}
			}
		} else {
			System.out.println("Wifi is not present in the list");
		}
		return isWifiConnected;
	}

	// Below are payment page objects which are common for locals, hotels and
	// flights multicity
	@FindBy(id = "com.cleartrip.android:id/btnCreditNormal")
	private WebElement cardAsPaymentOption;

	@FindBy(id = "com.cleartrip.android:id/btnNetbankingNormal")
	private WebElement netBankingAsPaymentOption;

	@FindBy(id = "com.cleartrip.android:id/bankWallet")
	private WebElement thirdPartyWalletAsPaymentOption;

	@FindBy(id = "com.cleartrip.android:id/card_number")
	private WebElement creditCardNumber;

	@FindBy(id = "com.cleartrip.android:id/expiryDate")
	private WebElement expiryMonthAndYear;

	@FindBy(id = "com.cleartrip.android:id/credit_cvv")
	private WebElement cvv;

	@FindBy(id = "com.cleartrip.android:id/btnSubmitPayment")
	private WebElement payButton;

	@FindBy(id = "android:id/button1")
	private WebElement expiryDialogueOkButton;

	@FindBy(xpath = "//android.widget.Button[@text='Submit']")
	private WebElement authenticationInPayment;

	@FindBy(id = "com.cleartrip.android:id/lytNetBanking")
	private WebElement netBankingList;
	
	@FindBy(id="com.cleartrip.android:id/cardNo")
	private WebElement newCreditCardFieldID;
	
	@FindBy(id="com.cleartrip.android:id/expiry")
	private WebElement expiryMonthAndYearNewId;
	
	@FindBy(id="com.cleartrip.android:id/payment_book_button")
	private WebElement payButtonNewId;
	
	@FindBy(id="com.cleartrip.android:id/cvv")
	private WebElement cvvInNewDesignPage;
	
	
	@FindBy(id="com.cleartrip.android:id/view_trip_details_trip_confirmation")
	private WebElement viewTripDetailsButton;
	
	@FindBy(xpath="//android.widget.TextView[@text='OKAY']")
	private WebElement okayPopUpInTripDetailsPage;
	
	@FindBy(id="com.cleartrip.android:id/btn_cancel")
	private WebElement cancelButtonInTripDetailsPage;
	
	@FindBy(xpath="//android.widget.Button[@text='Yes, cancel now']")
	private WebElement yesCancelNowButton;
	
	@FindBy(xpath="//android.view.View[@text='Your hotel booking was cancelled successfully']")
	private WebElement cancellationConfirmationPage;
	
	
	public WebElement getViewTripDetailsButton() {
		return viewTripDetailsButton;
	}

	public WebElement getOkayPopUpInTripDetailsPage() {
		return okayPopUpInTripDetailsPage;
	}

	public WebElement getCancelButtonInTripDetailsPage() {
		return cancelButtonInTripDetailsPage;
	}

	public WebElement getYesCancelNowButton() {
		return yesCancelNowButton;
	}

	public WebElement getCancellationConfirmationPage() {
		return cancellationConfirmationPage;
	}

	

	public WebElement getCvvInNewDesignPage() {
		return cvvInNewDesignPage;
	}

	public WebElement getPayButtonNewId() {
		return payButtonNewId;
	}

	public WebElement getCvv() {
		return cvv;
	}

	public WebElement getExpiryMonthAndYearNewId() {
		return expiryMonthAndYearNewId;
	}

	public WebElement getNewCreditCardFieldID() {
		return newCreditCardFieldID;
	}

	public WebElement getNetBankingBank() throws IOException {
		CommonUtil util = new CommonUtil();
		WebElement netBankingName = driver.findElement(
				By.xpath("//android.widget.TextView[@text='" + util.getCommonData("netbankingbank") + "'" + "]"));
		return netBankingName;

	}

	public WebElement getNetBankingList() {
		return netBankingList;
	}

	public WebElement getCardAsPaymentOption() {
		return cardAsPaymentOption;
	}

	public WebElement getNetBankingAsPaymentOption() {
		return netBankingAsPaymentOption;
	}

	public WebElement getThirdPartyWalletAsPaymentOption() {
		return thirdPartyWalletAsPaymentOption;
	}

	public WebElement getCreditCardNumber() {
		return creditCardNumber;
	}

	public WebElement getExpiryMonthAndYear() {
		return expiryMonthAndYear;
	}

	public WebElement getcvv() {
		return cvv;
	}

	public WebElement getPayButton() {
		return payButton;
	}

	public WebElement getExpiryDialogueOkButton() {
		return expiryDialogueOkButton;
	}

	public WebElement getAuthenticationInPayment() {
		return authenticationInPayment;
	}

	public void selectWallet(AppiumDriver driver, String paymentOption) {

		List<WebElement> walletList = new ArrayList<>();
		walletList = thirdPartyWallets(driver, paymentOption);

		switch (paymentOption.toLowerCase()) {
		case "paytm":
			walletList.get(2).click();
            break;
		case "payzapp":
			walletList.get(1).click();
            break;
		case "masterpass":
			walletList.get(2).click();
            break;
		case "payumoney":
			walletList.get(3).click();
            break;
		case "olamoney":
			walletList.get(4).click();
			break;
		case "phonepe":
			walletList.get(5).click();
			break;
		}

	}

	public List<WebElement> thirdPartyWallets(AppiumDriver driver, String paymentOption) {

		List walletList = new ArrayList<>();
		walletList = driver.findElements(By.id("com.cleartrip.android:id/wallet_row_layout"));

		return walletList;
	}

}
