package com.cleartrip.android.common;

import java.io.File;
import java.io.IOException;

import org.testng.TestNG;

import com.cleartrip.android.common.PropertyUtil;

public class Main extends TestNG {
	
	public static String env ;
	
	public static void main( String []args){
		String testType = "";
		String product ="";
		String modules = "";
		
		if(args.length == 0 ){
			env="qa2";
		}else if(args.length==1){
			env=args[0];
		}else if(args.length ==2){
			env = args[0];
			testType = args[1];
		}else if(args.length ==3){
			env = args[0];
			testType = args[1];
			product = args[2];
		}else{
			env = args[0];
			testType = args[1];
			product = args[2];
			modules = args[3];
			
		}
		
	String testSuite="";
	if(product!=null&&!product.isEmpty()){
		testSuite = System.getProperty("user.dir")+File.separator+product.toLowerCase()+".xml";
	}else{
		testSuite = System.getProperty("user.dir")+File.separator+"testng.xml";
	}
	
	String[] testNgRunArgs = new String[args.length];

	if (args.length == 0) {
	    testNgRunArgs = new String[args.length + 1];
	    testNgRunArgs[0] = testSuite;
	} else if (args.length == 1) {
	    testNgRunArgs = new String[args.length];
	    testNgRunArgs[0] = testSuite;
	} else if (args.length == 2) {
	    testNgRunArgs = new String[args.length + 1];
	    testNgRunArgs[0] = testSuite;
	    testNgRunArgs[1] = "-groups";
	    testNgRunArgs[2] = testType;
	} else if (args.length == 4) {
	    testNgRunArgs = new String[args.length + 1];
	    testNgRunArgs[0] = testSuite;
	    testNgRunArgs[1] = "-groups";
	    testNgRunArgs[2] = testType;
	    testNgRunArgs[3] = "-testnames";
	    testNgRunArgs[4] = modules;
	} else {
	    testNgRunArgs = new String[args.length];
	    testNgRunArgs[0] = testSuite;
	    testNgRunArgs[1] = "-groups";
	    testNgRunArgs[2] = testType;
	}
	
	TestNG.main(testNgRunArgs);
	
		
	}

	public static String getEnv(String s) throws IOException {

		String envPlaceHolderValue = null;
		if (env == null) {
			env = PropertyUtil.getCommonData("env");
		}	
			switch (env.toLowerCase()) {
			case "prod":
				envPlaceHolderValue = "www";
				break;
			case "beta":
				envPlaceHolderValue = "beta";
				break;
			case "hf":
				envPlaceHolderValue = "hf";
				break;
			case "dev":
				envPlaceHolderValue = "dev";
				break;
			case "qa":
				envPlaceHolderValue = "qa2";
				break;
			case "staging":
				envPlaceHolderValue = "staging";
				break;

			default:
				throw new IllegalArgumentException(
						"Invalid test run environment. env = '" + env + "'. Valid value are QA, Prod, beta, hf, dev ");
			}
		return envPlaceHolderValue;

	}
	
	

}
