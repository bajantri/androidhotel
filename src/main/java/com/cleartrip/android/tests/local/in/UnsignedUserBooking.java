package com.cleartrip.android.tests.local.in;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.cleartrip.android.common.Base;
import com.cleartrip.android.common.CleartripLandingPageObjects;
import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.objects.local.ActivitiesCheckAvaliablityPageObjects;
import com.cleartrip.android.objects.local.ActivitiesCollectionPageObjects;
import com.cleartrip.android.objects.local.ActivitiesDetailPageObjects;
import com.cleartrip.android.objects.local.ActivitiesListPageObjects;
import com.cleartrip.android.objects.local.common.ConformationPageObjects;
import com.cleartrip.android.objects.local.common.ItineraryPageObjects;
import com.cleartrip.android.objects.local.common.TripeDetailandCancellationPageObjects;

public class UnsignedUserBooking extends Base {

	@Test()
	public void searchPage() throws Exception {

		CleartripLandingPageObjects clp = new CleartripLandingPageObjects(getDriver());
	//	clp.clickPopUp(getDriver(), By.id("com.android.packageinstaller:id/dialog_container"), true);

		
		CommonUtil util = new CommonUtil();
		util.waitForElement(getDriver(), By.id("com.cleartrip.android:id/classic_bottom_navigation_container"));
		clp.switchProduct(getDriver(), "Activities");
		util.waitForElement(getDriver(), By.id("com.cleartrip.android:id/header"));
		
		ActivitiesCollectionPageObjects activity = new ActivitiesCollectionPageObjects(getDriver());
		activity.selectCity(getDriver());
		activity.selectAllActivity(getDriver(),"Explore all activities");
		activity.selectCollectionOrEditorials(getDriver(),"Festival-Tour");
		

		ActivitiesListPageObjects listpage = new ActivitiesListPageObjects(getDriver());
		//listpage.SelectlistPageActivity(getDriver());

		ActivitiesDetailPageObjects detailPage = new ActivitiesDetailPageObjects(getDriver());
		detailPage.clickBookNowButton(getDriver());
	
		ActivitiesCheckAvaliablityPageObjects checkAvaliablity = new ActivitiesCheckAvaliablityPageObjects(getDriver());
		checkAvaliablity.selectDateTime(getDriver(), "1", "0", false);

	
		ItineraryPageObjects itineraryPage = new ItineraryPageObjects(getDriver());
		itineraryPage.fillTravellerDetails(getDriver());
		itineraryPage.makePayment(getDriver());
		itineraryPage.fillCardDetail(getDriver());
		//itineraryPage.clickPayButton(getDriver());
		
		ConformationPageObjects conformationPage = new ConformationPageObjects(getDriver());
		conformationPage.clickViewTripDetail(getDriver());
		
		TripeDetailandCancellationPageObjects tripDetail = new TripeDetailandCancellationPageObjects(getDriver());
		tripDetail.getTripId(getDriver());

	}
}
