package com.cleartrip.android.tests.local.in;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import com.cleartrip.android.common.Base;
import com.cleartrip.android.common.CleartripLandingPageObjects;
import com.cleartrip.android.common.CommonPageObjects;
import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.common.Main;
import com.cleartrip.android.objects.accounts.SignInAndRegisterPageObjects;
import com.cleartrip.android.objects.local.ActivitiesCheckAvaliablityPageObjects;
import com.cleartrip.android.objects.local.ActivitiesCollectionPageObjects;
import com.cleartrip.android.objects.local.ActivitiesDetailPageObjects;
import com.cleartrip.android.objects.local.ActivitiesListPageObjects;
import com.cleartrip.android.objects.local.common.ConformationPageObjects;
import com.cleartrip.android.objects.local.common.ItineraryPageObjects;
import com.cleartrip.android.objects.local.common.TripeDetailandCancellationPageObjects;

public class SignedUserBooking extends Base {

	@Test()
	public void searchPage() throws Exception {

		CleartripLandingPageObjects clp = new CleartripLandingPageObjects(getDriver());
		//clp.travelLocalSwitch(getDriver(), By.id("com.android.packageinstaller:id/dialog_container"), "travel");
		CommonUtil util = new CommonUtil();
		util.waitForElement(getDriver(), By.id("com.cleartrip.android:id/classic_bottom_navigation_container"));
		clp.switchProduct(getDriver(), "You");
		SignInAndRegisterPageObjects signin=new SignInAndRegisterPageObjects(getDriver());
		signin.signIn(getDriver());
		
		clp.switchProduct(getDriver(), "Activities");
		//clp.clickPopUp(getDriver(), true);
		//clp.switchProduct(getDriver(), "You");
		
		//SignInAndRegisterPageObjects signInPage = new SignInAndRegisterPageObjects(getDriver());
		//signInPage.signInCredentials();
		//clp.switchProduct(getDriver(), "Activities");
		
		//CommonUtil util = new CommonUtil();
		util.waitForElement(getDriver(), By.id("com.cleartrip.android:id/header"));
		
		
		ActivitiesCollectionPageObjects activity = new ActivitiesCollectionPageObjects(getDriver());
		activity.selectCity(getDriver());
		activity.selectAllActivity(getDriver(),"Explore all activities");
		activity.selectCollectionOrEditorials(getDriver(),"Festival-Tour");

		//ActivitiesListPageObjects listpage = new ActivitiesListPageObjects(getDriver());
		//listpage.selectListPageActivity(getDriver());

		ActivitiesDetailPageObjects detailPage = new ActivitiesDetailPageObjects(getDriver());
		detailPage.clickBookNowButton(getDriver());
	
		ActivitiesCheckAvaliablityPageObjects checkAvaliablity = new ActivitiesCheckAvaliablityPageObjects(getDriver());
		checkAvaliablity.selectDateTime(getDriver(), "1", "0", false);

	
		ItineraryPageObjects itineraryPage = new ItineraryPageObjects(getDriver());
		itineraryPage.fillTravellerDetailsSignedIn(getDriver());
		itineraryPage.makePayment(getDriver());
		
		CommonPageObjects cp =new CommonPageObjects(getDriver());
		cp.getUseNewCardLinkInPaymentPage().click();
		 
		itineraryPage.fillCardDetailSignedIn(getDriver());
		//itineraryPage.payNowButton(getDriver());
		
		
		ConformationPageObjects conformationPage = new ConformationPageObjects(getDriver());
		conformationPage.clickViewTripDetail(getDriver());
		
		TripeDetailandCancellationPageObjects tripDetail = new TripeDetailandCancellationPageObjects(getDriver());
		tripDetail.getTripId(getDriver());

	}
}
