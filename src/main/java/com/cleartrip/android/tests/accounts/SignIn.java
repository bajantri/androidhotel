package com.cleartrip.android.tests.accounts;

import java.io.IOException;

import org.testng.annotations.Test;

import com.cleartrip.android.common.Base;
import com.cleartrip.android.common.CleartripLandingPageObjects;
import com.cleartrip.android.objects.accounts.SignInAndRegisterPageObjects;
import com.cleartrip.android.objects.accounts.YouPageObjects;

public class SignIn extends Base {

	@Test
	public void signIn() throws InterruptedException, IOException{
		CleartripLandingPageObjects clp = new CleartripLandingPageObjects(getDriver());
		clp.clickPopUp(getDriver(), true);
		clp.switchProduct(getDriver(), "You");
		
		YouPageObjects ypo = new YouPageObjects(getDriver());
		ypo.getSignInOrRegisterButton();
		
		SignInAndRegisterPageObjects sir = new SignInAndRegisterPageObjects(getDriver());
		sir.signIn(getDriver());
	}
}
