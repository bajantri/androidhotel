package com.cleartrip.android.tests.hotels.in.dom;

import static org.testng.Assert.assertTrue;

import java.sql.Driver;
import java.text.DateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.server.DriverFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.cleartrip.android.common.Base;
import com.cleartrip.android.common.CleartripLandingPageObjects;
import com.cleartrip.android.common.CommonPageObjects;
import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.objects.accounts.SignInAndRegisterPageObjects;
import com.cleartrip.android.objects.accounts.YouPageObjects;
import com.cleartrip.android.objects.hotel.HotelDetailsPageObjects;
import com.cleartrip.android.objects.hotel.HotelHomePageObjects;
import com.cleartrip.android.objects.hotel.HotelItineraryPageObjects;
import com.cleartrip.android.objects.hotel.HotelSearchResultsPageObjects;
import com.cleartrip.android.objects.hotel.PaymentPageObjects;
import com.cleartrip.android.objects.hotel.RoomSelectionPageObjects;
import com.gargoylesoftware.htmlunit.javascript.host.Element;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.remote.YouiEngineCapabilityType;

public class SignedInCreditCardBooking extends Base {
	CommonUtil util = new CommonUtil();

	@Test()
	public void searchPage() throws Exception {

		CleartripLandingPageObjects ct = new CleartripLandingPageObjects(getDriver());
		ct.travelLocalSwitch(getDriver(), By.id("com.android.packageinstaller:id/dialog_container"), "travel");
		SignInAndRegisterPageObjects signin=new SignInAndRegisterPageObjects(getDriver());
		YouPageObjects yu= new YouPageObjects(getDriver());
		Thread.sleep(3000);
		yu.getYouTab().click();
		signin.signIn(getDriver());
		
		ct.switchProduct(getDriver(), "Hotels");

		// Hotel search form
		HotelHomePageObjects hm = new HotelHomePageObjects(getDriver());
		//hm.destinationAndSelectCity("U G Royal", 1);
		hm.destinationAndSelectCity("hotel vt orchid", 1);

		// Hotel details page
		HotelDetailsPageObjects detailPage = new HotelDetailsPageObjects(getDriver());
		detailPage.selectRoomButtonInDetailsPage();

		// Room selection page
		RoomSelectionPageObjects selectRoom = new RoomSelectionPageObjects(getDriver());
		selectRoom.bookButton(getDriver());

		// Traveler details
		HotelItineraryPageObjects itinerary = new HotelItineraryPageObjects(getDriver());
		itinerary.travelerFillForSignin();
		itinerary.selectPaymentType(getDriver(), "");
		CommonPageObjects cp =new CommonPageObjects(getDriver());
		cp.getUseNewCardLinkInPaymentPage().click();
		

		// Payment and coupon details page
		CommonUtil util = new CommonUtil();
		
		if(util.isPayment()==true) {
		util.makePayment(getDriver(), "creditcard", "mastercard", "");
		util.contextSwitch(getDriver());
		util.waitForElement(getDriver(), By.id("com.cleartrip.android:id/booking_trip_id_content_trip_confirmation"));
		PaymentPageObjects pm =new PaymentPageObjects(getDriver());
		String tripId =pm.getTripId().getText();
		Reporter.log(tripId);
		}else {
			PaymentPageObjects pm=new PaymentPageObjects(getDriver());
			pm.creditCardPayment();
		}
		
       // util.contextSwitch(getDriver());
		

	}

}
