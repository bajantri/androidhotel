package com.cleartrip.android.tests.hotels.in.dom;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.cleartrip.android.common.Base;
import com.cleartrip.android.common.CleartripLandingPageObjects;
import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.objects.hotel.HotelDetailsPageObjects;
import com.cleartrip.android.objects.hotel.HotelHomePageObjects;
import com.cleartrip.android.objects.hotel.HotelItineraryPageObjects;
import com.cleartrip.android.objects.hotel.PaymentPageObjects;
import com.cleartrip.android.objects.hotel.RoomSelectionPageObjects;

public class NetBankingRetryWithCreditCardPayment extends Base {

	@Test()
	public void searchPage() throws Exception {

		CleartripLandingPageObjects ct = new CleartripLandingPageObjects(getDriver());
		ct.travelLocalSwitch(getDriver(), By.id("com.android.packageinstaller:id/dialog_container"), "travel");
		ct.switchProduct(getDriver(), "Hotels");

		// Hotel search form
		HotelHomePageObjects hm = new HotelHomePageObjects(getDriver());
		hm.destinationAndSelectCity("Hotel Sitara Paradise", 1);

		// Hotel details page
		HotelDetailsPageObjects detailPage = new HotelDetailsPageObjects(getDriver());
		detailPage.selectRoomButtonInDetailsPage();

		// Room selection page
		RoomSelectionPageObjects selectRoom = new RoomSelectionPageObjects(getDriver());
		selectRoom.bookButton(getDriver());
		

		// Traveler details
		HotelItineraryPageObjects itinerary = new HotelItineraryPageObjects(getDriver());
		itinerary.fillTravellerDetails(getDriver());
		itinerary.selectPaymentType(getDriver(), "");

		// Payment and coupon details page
		CommonUtil util = new CommonUtil();
		if (util.isPayment() == true) {
			util.netBankRetry(getDriver());
			Thread.sleep(2000);
			util.makePayment(getDriver(), "creditcard", "mastercard", "");
			util.contextSwitch(getDriver());
			util.waitForElement(getDriver(),
					By.id("com.cleartrip.android:id/booking_trip_id_content_trip_confirmation"));
			PaymentPageObjects pm = new PaymentPageObjects(getDriver());
			String tripId = pm.getTripId().getText();
			Reporter.log(tripId);
		} else {
			PaymentPageObjects pm = new PaymentPageObjects(getDriver());
			pm.creditCardPayment();
		}
		
		
		
		
        //util.contextSwitch(getDriver());
		

}}
