package com.cleartrip.android.tests.hotels.in.dom;

import static org.testng.Assert.assertTrue;

import java.sql.Driver;
import java.text.DateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.DriverFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.cleartrip.android.common.Base;
import com.cleartrip.android.common.CleartripLandingPageObjects;
import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.objects.accounts.YouPageObjects;
import com.cleartrip.android.objects.hotel.HotelDetailsPageObjects;
import com.cleartrip.android.objects.hotel.HotelHomePageObjects;
import com.cleartrip.android.objects.hotel.HotelItineraryPageObjects;
import com.cleartrip.android.objects.hotel.HotelSearchResultsPageObjects;
import com.cleartrip.android.objects.hotel.PaymentPageObjects;
import com.cleartrip.android.objects.hotel.RoomSelectionPageObjects;
import com.gargoylesoftware.htmlunit.javascript.host.Element;

import io.appium.java_client.AppiumDriver;

public class SaDomainBookingWithCreditCard extends Base {
	CommonUtil util = new CommonUtil();

	@Test()
	public void searchPage() throws Exception {

		CleartripLandingPageObjects ct = new CleartripLandingPageObjects(getDriver());
		ct.travelLocalSwitch(getDriver(), By.id("com.android.packageinstaller:id/dialog_container"), "travel");
		YouPageObjects you = new YouPageObjects(getDriver());
		you.selectCountry(getDriver(), "Saudi Arabia");
		ct.switchProduct(getDriver(), "Hotels");

		// Hotel search form
		HotelHomePageObjects hm = new HotelHomePageObjects(getDriver());
		hm.destinationAndSelectCity("The Vijay Park", 1);

		// Hotel details page
		HotelDetailsPageObjects detailPage = new HotelDetailsPageObjects(getDriver());
		detailPage.selectRoomButtonInDetailsPage();

		// Room selection page
		RoomSelectionPageObjects selectRoom = new RoomSelectionPageObjects(getDriver());
		selectRoom.getContinueBookingButton().click();

		// Traveler details
		HotelItineraryPageObjects itinerary = new HotelItineraryPageObjects(getDriver());
		itinerary.fillTravellerDetails(getDriver());
		itinerary.selectPaymentType(getDriver(), "");

		// Payment and coupon details page
		CommonUtil util = new CommonUtil();
		/*
		 * if(util.isPayment()==true) { PaymentPageObjects pay = new
		 * PaymentPageObjects(getDriver()); util.makePayment(getDriver(), "creditcard",
		 * "mastercard",""); util.waitForElement(getDriver(),
		 * By.id("com.cleartrip.android:id/booking_trip_id_content_trip_confirmation"));
		 * PaymentPageObjects pm =new PaymentPageObjects(getDriver()); pm.getTripId(); }
		 * else {
		 */
		PaymentPageObjects pm = new PaymentPageObjects(getDriver());
		pm.creditCardPayment();

		// util.contextSwitch(getDriver());
		// util.makePayment(getDriver(), "netbanking", "");
		// util.makePayment(getDriver(), "thirdpartywallet", "paytm");

	}

}
