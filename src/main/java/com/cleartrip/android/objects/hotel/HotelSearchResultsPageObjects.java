package com.cleartrip.android.objects.hotel;

import static org.testng.Assert.assertTrue;

import java.sql.Driver;
import java.util.List;

import javax.rmi.CORBA.Util;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.cleartrip.android.common.Base;
import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.common.PageObject;

import io.appium.java_client.AppiumDriver;

public class HotelSearchResultsPageObjects extends PageObject {

	public HotelSearchResultsPageObjects(AppiumDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@FindBy(id = "com.cleartrip.android:id/hotel_360_okay_button")
	private WebElement threeSixtydegreePopUp;

	@FindBy(id = "com.cleartrip.android:id/filterTxt")
	private WebElement filterIcon;

	@FindBy(id = "com.cleartrip.android:id/checkBoxFreeCancelApplied")
	private WebElement freeCancelCheckBox;

	@FindBy(id = "com.cleartrip.android:id/hotelfliterApplyBtn")
	private WebElement payAtHotelCheckBox;

	@FindBy(id = "com.cleartrip.android:id/hotelfliterApplyBtn")
	private WebElement applyButton;

	@FindBy(id = "com.cleartrip.android:id/hotelBtnResetFilters")
	private WebElement resetAllButton;

	@FindBy(xpath = "//android.widget.TextView[@index='1']")
	private WebElement searchedResultPage;

	@FindBy(id = "com.cleartrip.android:id/starRating3")
	private WebElement starRating;

	@FindBy(id = "com.cleartrip.android:id/tadRating3")
	private WebElement tripadvisorRating;

	@FindBy(id = "com.cleartrip.android:id/txtHotelName")
	private WebElement selectFirstHotel;

	@FindBy(id="com.cleartrip.android:id/sortTxt")
	private WebElement sortingFilter;
	
	@FindBy(id="com.cleartrip.android:id/priceLowTxt")
	private  WebElement lowToHighPrice;
	
	@FindBy(id="com.cleartrip.android:id/tagsLayout")
	private WebElement tagsIcon;
	
	
	
	public WebElement getThreeSixtydegreePopUp() {
		return threeSixtydegreePopUp;
	}

	public WebElement getTagsIcon() {
		return tagsIcon;
	}

	public WebElement getSortingFilter() {
		return sortingFilter;
	}

	public WebElement getLowToHighPrice() {
		return lowToHighPrice;
	}

	public WebElement getSelectFirstHotel() {
		return selectFirstHotel;
	}

	public WebElement getStarRating() {
		return starRating;
	}

	public WebElement getTripadvisorRating() {
		return tripadvisorRating;
	}

	public WebElement getThreeSixtydegrePopUp() {
		return threeSixtydegreePopUp;
	}

	public void srpPopUp(AppiumDriver driver) throws InterruptedException {
		CommonUtil util = new CommonUtil();
		Thread.sleep(2000);
		if(driver.findElement(By.xpath("//android.widget.Button[@text='OKAY, GOT IT']")).isDisplayed()) {
		
			getThreeSixtydegrePopUp().click();
		}
		//util.waitForElement(driver, By.xpath("//android.widget.Button[@text='OKAY, GOT IT']"));
		else {
			
		}
	}

	public WebElement getFilterIcon() {
		return filterIcon;
	}

	public WebElement getFreeCancelCheckBox() {
		return freeCancelCheckBox;
	}

	public WebElement getPayAtHotelCheckBox() {
		return payAtHotelCheckBox;
	}

	public WebElement getApplyButton() {
		return applyButton;
	}

	public WebElement getResetAllButton() {
		return resetAllButton;
	}

	public WebElement getSearchedResultPage() {
		return searchedResultPage;
	}

	public void selctHotel(AppiumDriver driver) {
		CommonUtil util=new CommonUtil();
		util.waitForElement(driver, By.id("com.cleartrip.android:id/sortTxt"));
		getSortingFilter().click();
		util.waitForElement(driver, By.id("com.cleartrip.android:id/priceLowTxt"));
		getLowToHighPrice().click();
		//driver.findElement(By.id("com.cleartrip.android:id/txtHotelName")).click();
		getSelectFirstHotel().click();

	}
	
	public void tagsPopUp(AppiumDriver driver) {
		CommonUtil util=new CommonUtil();
		util.waitForElement(driver, By.id("com.cleartrip.android:id/tagsLayout"));
		//driver.findElement(By.xpath("//android.widget.TextView[@index='5']")).click();
		
		getTagsIcon().click();
		
		
		
	}
	
	public void hotelTaggingSelect() {
		List tags = driver.findElements(By.xpath("//android.widget.TextView"));
		System.out.println("No of tags present :::: "+tags.size());
			}
	
	
	
	public boolean  assertValidation(AppiumDriver driver ) {
		boolean a = driver.findElement(By.id("com.cleartrip.android:id/sortTxt")).isDisplayed();
		
		boolean assertResult = false ;
		Assert.assertTrue(a);
		/*if(by.toString()!=null || by.toString().isEmpty()) {
			Assert.assertTrue(a,"srp page passed");
		
		assertResult = true;
		}*/
		return assertResult;
		
	}

}
