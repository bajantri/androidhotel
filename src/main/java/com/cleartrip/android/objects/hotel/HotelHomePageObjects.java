package com.cleartrip.android.objects.hotel;

import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.annotation.WebListener;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.remote.server.handler.interactions.touch.Scroll;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.common.PageObject;
import com.gargoylesoftware.htmlunit.javascript.host.Element;
import com.gargoylesoftware.htmlunit.javascript.host.Touch;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;

public class HotelHomePageObjects extends PageObject {

	public HotelHomePageObjects(AppiumDriver driver) {
		super(driver);

	}

	@FindBy(id = "com.cleartrip.android:id/lyt_from")
	private WebElement destination;

	@FindBy(id = "com.cleartrip.android:id/filter_edittext")
	private WebElement enterDestination;

	@FindBy(id = "com.cleartrip.android:id/lyt_checkin_date")
	private WebElement checkInDate;

	@FindBy(id = "com.cleartrip.android:id/checkOut")
	private WebElement checkOutDate;

	@FindBy(id = "com.cleartrip.android:id/lyt_travellers_class")
	private WebElement travellers;

	@FindBy(id = "com.cleartrip.android:id/btn_search_hotels")
	private WebElement searchButton;

	@FindBy(xpath = "//android.widget.TextView[@text='6']")
	private WebElement checkInDateSelection;

	@FindBy(xpath = "//android.widget.TextView[@text='7']")
	private WebElement checkOutDateSelection;

	@FindBy(id = "com.cleartrip.android:id/localityName_in_setLocation")
	private WebElement selectCity;

	@FindBy(id = "com.cleartrip.android:id/txtIknowMyDates")
	private WebElement iKnowMyTravelDates;

	@FindBy(id = "com.cleartrip.android:id/filterFAB")
	private WebElement addRoom;

	@FindBy(id = "com.cleartrip.android:id/increamentBtn")
	private List<WebElement> addAdults;

	@FindBy(id = "com.cleartrip.android:id/increamentBtn")
	private List<WebElement> addChild;

	@FindBy(id = "com.cleartrip.android:drawable/tick_white")
	private WebElement okButton;

	@FindBy(id = "com.cleartrip.android:id/childAgeSpinner")
	private WebElement dropDownButton;

	@FindBy(id = "android:id/text1")
	private List<WebElement> childAge;
	
	@FindBy(id="com.cleartrip.android:id/dateCancelImgBorder")
	private WebElement dateCancelbtn;
	
	@FindBy(id="com.cleartrip.android:id/lyt_checkin_date")
	private WebElement checkInlayout;
	
	@FindBy(id="com.cleartrip.android:id/localityName_in_setLocation")
	private List<WebElement> cityDropDown;
	
	@FindBy(id="com.cleartrip.android:id/txtSimilarHotelsHeading")
	private WebElement similarHotelsHeading;
	
	@FindBy(xpath = "//android.widget.TextView[@text='8']")
	private WebElement checkOutDateSelectionForMultiNight;
	
	@FindBy(xpath = "//android.widget.TextView[@text='24']")
	private WebElement checkInDateSelectionInDetailsPage;

	@FindBy(id="com.cleartrip.android:id/imvIncrementChildren")
	private WebElement addChildButtonNew;
	
	@FindBy(id="com.cleartrip.android:id/txtAddAnotherRoom")
	private WebElement addRoomButtonNew;
	
	@FindBy(id="com.cleartrip.android:id/txtAgeOne")
	private WebElement childAgeNew;
	
	@FindBy(id="com.cleartrip.android:id/btnDone")
	private WebElement doneButtonInPaxAddPage;
	
	
	public WebElement getDoneButtonInPaxAddPage() {
		return doneButtonInPaxAddPage;
	}

	public WebElement getAddChildButtonNew() {
		return addChildButtonNew;
	}

	public WebElement getAddRoomButtonNew() {
		return addRoomButtonNew;
	}

	public WebElement getChildAgeNew() {
		return childAgeNew;
	}

	public WebElement getCheckInDateSelectionInDetailsPage() {
		return checkInDateSelectionInDetailsPage;
	}

	public WebElement getCheckOutDateSelectionInDetailspage() {
		return checkOutDateSelectionInDetailspage;
	}

	@FindBy(xpath = "//android.widget.TextView[@text='25']")
	private WebElement checkOutDateSelectionInDetailspage;

	
	
	
	public WebElement getCheckOutDateSelectionForMultiNight() {
		return checkOutDateSelectionForMultiNight;
	}

	public WebElement getSimilarHotelsHeading() {
		return similarHotelsHeading;
	}

	public List<WebElement> getCityDropDown() {
		return cityDropDown;
	}

	public WebElement getCheckInlayout() {
		return checkInlayout;
	}

	public List<WebElement> getAddAdults() {
		return addAdults;
	}

	public List<WebElement> getChildAge() {
		return childAge;
	}

	public WebElement getDateCancelbtn() {
		return dateCancelbtn;
	}

	public WebElement getDropDownButton() {
		dropDownButton.click();
		return dropDownButton;
	}

	public List<WebElement> getChildAge(AppiumDriver driver) {
		System.out.println("count of the child age in popup is ::::::::" + childAge.size());
		childAge.get(3).click();
		return childAge;
	}

	public WebElement getAddRoom() {
		return addRoom;
	}

	public List<WebElement> getAddAdults(AppiumDriver driver) {
		System.out.println("size of the buttons in traveler page is :::::::::::::" + addAdults.size());
		addAdults.get(2).click();
		return addAdults;
	}

	public List<WebElement> getAddChild() {
		addChild.get(3).click();
		return addChild;
	}

	public WebElement getOkButton() {
		return okButton;
	}

	public WebElement getiKnowMyTravelDates() {
		return iKnowMyTravelDates;
	}

	public WebElement getSelectCity() {
		return selectCity;
	}

	public WebElement getEnterDestination() {
		return enterDestination;
	}

	public WebElement getCheckOutDateSelection() {
		return checkOutDateSelection;
	}

	public WebElement getCheckInDateSelection() {
		return checkInDateSelection;
	}

	public WebElement getDestination() {

		return destination;
	}

	public WebElement getCheckInDate() {
		return checkInDate;
	}

	public WebElement getCheckOutDate() {
		return checkOutDate;
	}

	public WebElement getTravellers() {
		return travellers;
	}

	public WebElement getSearchButton() {
		return searchButton;
	}

	public void datePicker(AppiumDriver driver) throws InterruptedException {
		
		Thread.sleep(3000);
		getCheckInDate().click();
		CommonUtil cutil = new CommonUtil();
		cutil.dateSelection(driver);
		//cutil.shortSwipe(driver, 3);
		cutil.swipeDown(driver, "may 2019");
		getCheckInDateSelection().click();
		//cutil.waitForElement(driver, By.xpath("//android.widget.TextView[@text='23']"));
		getCheckOutDate().click();
		getCheckOutDateSelection().click();

	}

	public void selectingNoOfRooms(AppiumDriver driver) {
		CommonUtil cutil = new CommonUtil();
		getTravellers().click();
		getAddRoom().click();
		getAddAdults(driver);
		getAddChild();
		getDropDownButton();
		getChildAge(driver);
		getOkButton().click();

	}

	public void destinationAndSelectCity(String city, int noOfRooms) throws InterruptedException {
		getDestination().click();
		CommonUtil util = new CommonUtil();
		util.waitForElement(driver, By.id("com.cleartrip.android:id/filter_edittext"));
		getEnterDestination().sendKeys(city);
		Thread.sleep(2000);
		util.waitForElement(driver, By.id("com.cleartrip.android:id/localityName_in_setLocation"));
		getSelectCity().click();
		if (noOfRooms >= 2) {
			selectingNoOfRooms(driver);
		} else {

		}
		
		datePicker(driver);
		getSearchButton().click();
	}

	public void recentSearchedHotel(AppiumDriver driver) throws InterruptedException {
		// check in and check out from srp page
		CommonUtil util =new CommonUtil();
		util.waitForElement(driver, By.xpath("//android.widget.TextView[@index='2']"));
		//WebElement a = driver.findElement(By.id("com.cleartrip.android:id/filterTxt"));
		 WebElement a = driver.findElement(By.xpath("//android.widget.TextView[@index='2']"));
		  String b = driver.findElement(By.xpath("//android.widget.TextView[@index='1']")).getText();
		System.out.println("date in srp b is ::::"+  b);
		if (a.isDisplayed()) {
			String dateInSrpPage = driver.findElement(By.xpath("//android.widget.TextView[@index='2']")).getText();
			System.out.println("Dates in srp page is :::"+ dateInSrpPage);
			String[] checkInAndCheckOutdate = dateInSrpPage.split(",");
			System.out.println("text at index 0 is ::"+checkInAndCheckOutdate[0]);
			String[] cinAndCout = checkInAndCheckOutdate[0].split("-");
			String checkInDateSrp = cinAndCout[0].toString();
			String checkOutDateSrp = cinAndCout[1].toString();
			String datesFromSearchResultsPage = checkInDateSrp + checkOutDateSrp;
			System.out.println("After removing - " + datesFromSearchResultsPage);

			System.out.println("Text after editing " + checkInAndCheckOutdate[0]);

			driver.navigate().back();

			// New hotel home 2.0 home page clicking on recent searches
			scrollUp(driver, By.id("com.cleartrip.android:id/btn_search_hotels"));
			String checkInDate = driver.findElement(By.id("com.cleartrip.android:id/txtCheckInDate")).getText();
			String checkOutDate = driver.findElement(By.id("com.cleartrip.android:id/txtCheckOutDate")).getText();
			String date = checkInDate + "  " + checkOutDate;
			System.out.println(date);

			if (date.equalsIgnoreCase(datesFromSearchResultsPage)) {
				System.out.println("Dates are  matching from home page");
			} else {
				System.out.println("Dates are not matching from home page ");
			}

			driver.findElement(By.id("com.cleartrip.android:id/txtArrow")).click();

		}
	}
	
	public void dateLessSearch(AppiumDriver driver , String city) throws InterruptedException {
		if (getCheckInlayout().isDisplayed()) {
			getDateCancelbtn().click();
			CommonUtil util = new CommonUtil();
			getDestination().click();
			util.waitForElement(driver, By.id("com.cleartrip.android:id/filter_edittext"));
			getEnterDestination().sendKeys(city);
			getSelectCity().click();
			getSearchButton().click();
			
		}
		
	}
	
	public void dateSelectionForDateless(AppiumDriver driver) throws InterruptedException {
		CommonUtil cutil = new CommonUtil();
		HotelDetailsPageObjects hd= new HotelDetailsPageObjects(driver);
		cutil.waitForElement(driver, By.id("com.cleartrip.android:id/btnCheckAvailibility"));
		hd.getCheckAvailability().click();
		getCheckInDate().click();
		cutil.swipeDown(driver, cutil.dateSelection(driver));
		getCheckInDateSelection().click();
		cutil.waitForElement(driver, By.xpath("//android.widget.TextView[@text='23']"));
		getCheckOutDate().click();
		getCheckOutDateSelection().click();
		hd.getDoneButton().click();
		
		
		
	}
	
	public void cityDropDownList(AppiumDriver driver) throws InterruptedException {
		getDestination().click();
		CommonUtil util = new CommonUtil();
		util.waitForElement(driver, By.id("com.cleartrip.android:id/filter_edittext"));
		getEnterDestination().sendKeys("bangkok");
		driver.hideKeyboard();
		util.waitForElement(driver, By.id("com.cleartrip.android:id/localityName_in_setLocation"));
		cityDropDown.get(4).click();
		datePicker(driver);
		getSearchButton().click();
	}
	
	public void scrollUp(AppiumDriver driver, By by ) {
		Dimension size = driver.manage().window().getSize();
		boolean isElementFound = false;
		int height = size.height;
		System.out.println("height of the screen"+height);
		int width = size.width;
		System.out.println("Width of the screen"+width);
		int startY = (int) (height * 0.15);
		int endY = (int) (height * 0.75);
		int startX = (int) (width / 2);
		int endX = (int) (width / 2);
		WebElement elementPresent = null;
		TouchAction action = new TouchAction(driver);
		while (isElementFound == false) {
			action.press(startX, startY).waitAction(Duration.ofMillis(500)).moveTo(endX, endY).release().perform();
			try {
				if (driver.findElement(by).isDisplayed()) {
					elementPresent = driver.findElement(by);
					isElementFound = true;
					break;
				}
			} catch (Exception e) {
				System.out.println("Element not found");
			}
		}
			
		
	
	}
	
	public void datePickerForMultiNight(AppiumDriver driver, String city, int noOfRooms, String oldOrNewFlow)
			throws InterruptedException {
		getDestination().click();
		CommonUtil util = new CommonUtil();
		util.waitForElement(driver, By.id("com.cleartrip.android:id/filter_edittext"));
		getEnterDestination().sendKeys(city);
		getSelectCity().click();
		if (noOfRooms >= 2) {
			if (oldOrNewFlow.equalsIgnoreCase("oldflow")) {
				selectingNoOfRooms(driver);
			}
			 
			if (oldOrNewFlow.equalsIgnoreCase("newflow")) {
				addChildNewDesign(driver);}
			
		} 

		

		getCheckInDate().click();
		CommonUtil cutil = new CommonUtil();
		cutil.swipeDown(driver, cutil.dateSelection(driver));
		getCheckInDateSelection().click();
		cutil.waitForElement(driver, By.xpath("//android.widget.TextView[@text='23']"));
		getCheckOutDate().click();
		getCheckOutDateSelectionForMultiNight().click();
		getSearchButton().click();
		
		

	}
	
	public void addChildNewDesign(AppiumDriver driver) {
		getTravellers().click();
		getAddRoomButtonNew().click();
		getAddChildButtonNew().click();
		getChildAgeNew().sendKeys("7");
		driver.hideKeyboard();
		getDoneButtonInPaxAddPage().click();
		
	}
}
