package com.cleartrip.android.objects.hotel;

import java.io.IOException;
import java.sql.Driver;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.swing.plaf.basic.BasicSliderUI.ScrollListener;

import org.apache.bcel.generic.BREAKPOINT;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.interactions.touch.Scroll;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.support.GenericTypeAwareAutowireCandidateResolver;

import com.cleartrip.android.common.CommonPageObjects;
import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.common.PageObject;
import com.cleartrip.android.common.PropertyUtil;
import com.gargoylesoftware.htmlunit.html.Keyboard;
import com.gargoylesoftware.htmlunit.javascript.host.Element;
import com.gargoylesoftware.htmlunit.javascript.host.Window;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;

public class HotelItineraryPageObjects extends PageObject {

	public HotelItineraryPageObjects(AppiumDriver driver) {

		super(driver);
	}

	@FindBy(id = "com.cleartrip.android:id/hvb_booking_details")
	private WebElement bookingDetails;

	@FindBy(id = "com.cleartrip.android:id/nameTxt")
	private WebElement firstName;

	@FindBy(id = "com.cleartrip.android:id/lastNameTxt")
	private WebElement lastName;

	@FindBy(id = "com.cleartrip.android:id/emailTxt")
	private WebElement emailId;

	@FindBy(id = "com.cleartrip.android:id/phoneTxt")
	private WebElement phoneNumber;

	@FindBy(id = "com.cleartrip.android:id/fareBreakUp")
	private WebElement fareBreakUp;

	@FindBy(id = "com.cleartrip.android:id/signInTxt")
	private WebElement signInText;

	@FindBy(id = "com.cleartrip.android:id/expiryDate")
	private WebElement expDateAndYear;

	@FindBy(id = "com.cleartrip.android:id/monthLyt")
	private String abc;

	@FindBy(id = "com.cleartrip.android:id/pickTraveller")
	private WebElement pickTraveler;

	@FindBy(id = "com.cleartrip.android:id/travellerName")
	private WebElement autoFillTraveler;

	@FindBy(id = "com.cleartrip.android:id/traveller_header_text")
	private WebElement contactDetails;

	@FindBy(id = "com.cleartrip.android:id/btnSubmitPayment")
	private WebElement continueBookingButtonInTravellerPage;

	public WebElement getContinueBookingButtonInTravellerPage() {
		return continueBookingButtonInTravellerPage;
	}

	public WebElement getContactDetails() {
		return contactDetails;
	}

	public WebElement getPickTraveler() {
		return pickTraveler;
	}

	public WebElement getAutoFillTraveler() {
		return autoFillTraveler;
	}

	public String getAbc() {
		System.out.println("value at calendar" + abc);
		return abc;
	}

	public WebElement getExpDateAndYear() {
		return expDateAndYear;
	}

	public WebElement getBookingDetails() {
		return bookingDetails;
	}

	public WebElement getFirstName() {
		return firstName;
	}

	public WebElement getLastName() {
		return lastName;
	}

	public WebElement getEmailId() {
		return emailId;
	}

	public WebElement getPhoneNumber() {
		return phoneNumber;
	}

	public WebElement getFareBreakUp() {
		return fareBreakUp;
	}

	public WebElement getSignInText() {
		return signInText;
	}

	public void fillTravellerDetails(AppiumDriver driver) throws Exception {
		CommonUtil cmnutil = new CommonUtil();
		if (CommonUtil.getCommonData("newdesignorold").equalsIgnoreCase("oldflow")) {
			cmnutil.waitForElement(driver, By.id("com.cleartrip.android:id/traveller_header_text"));
			driver.hideKeyboard();
			cmnutil.swipeDownById(driver, By.id("com.cleartrip.android:id/phoneTxt"));
			getFirstName().sendKeys(PropertyUtil.getHotelData("firstname"));
			Thread.sleep(2000);
			driver.hideKeyboard();
			cmnutil.waitForElement(driver, By.id("com.cleartrip.android:id/lastNameTxt"));
			getLastName().sendKeys(PropertyUtil.getHotelData("lastname"));
			driver.hideKeyboard();
			cmnutil.swipeDownById(driver, By.id("com.cleartrip.android:id/emailTxt"));
			getEmailId().sendKeys(PropertyUtil.getHotelData("qaemail"));
			driver.hideKeyboard();
			// cmnutil.swipeDownById(driver,
			// By.id("com.cleartrip.android:id/phoneTxt")).getText();
			getPhoneNumber().sendKeys(PropertyUtil.getHotelData("mobnum"));
			driver.hideKeyboard();

		}

		else if (CommonUtil.getCommonData("newdesignorold").equalsIgnoreCase("newflow")) {

			CommonUtil util = new CommonUtil();
			//cmnutil.waitForElement(driver, By.id("com.cleartrip.android:id/traveller_header_text"));
			// driver.hideKeyboard();
			Thread.sleep(3000);
			cmnutil.swipeDownById(driver, By.id("com.cleartrip.android:id/phoneTxt"));
			getFirstName().sendKeys(PropertyUtil.getHotelData("firstname"));
			Thread.sleep(2000);
			driver.hideKeyboard();
			cmnutil.waitForElement(driver, By.id("com.cleartrip.android:id/lastNameTxt"));
			getLastName().sendKeys(PropertyUtil.getHotelData("lastname"));
			driver.hideKeyboard();
			cmnutil.swipeDownById(driver, By.id("com.cleartrip.android:id/emailTxt"));
			getEmailId().sendKeys(PropertyUtil.getHotelData("qaemail"));
			driver.hideKeyboard();
			// cmnutil.swipeDownById(driver,
			// By.id("com.cleartrip.android:id/phoneTxt")).getText();
			getPhoneNumber().sendKeys(PropertyUtil.getHotelData("mobnum"));
			driver.hideKeyboard();
			util.swipeDownById(driver, By.id("com.cleartrip.android:id/btnSubmitPayment"));

		}
	}

	public void selectPaymentType(AppiumDriver driver, String type) throws InterruptedException, IOException {

		CommonUtil util = new CommonUtil();
		if (CommonUtil.getCommonData("newdesignorold").equalsIgnoreCase("oldflow")) {
			util.swipeDownById(driver, By.id("com.cleartrip.android:id/signInTxt"));
			PaymentPageObjects payment = new PaymentPageObjects(driver);
			if (type.equalsIgnoreCase("fullamount")) {
				payment.getPayFullButton().click();

			}
			if (type.equalsIgnoreCase("partpay")) {
				if (payment.getPartPayButton().isDisplayed()) {
					System.out.println("Part pay option is enabled for this hotel");
				} else {
					System.out.println("Part pay option is NOT enabled for this hotel");
				}
				payment.getPartPayButton().click();
			}
			if (type.equalsIgnoreCase("pah")) {
				payment.getPayAtHotel().click();
			}
			if (type.equalsIgnoreCase("netbank")) {
				util.netBankRetry(driver);

			}

			else {
				if (type.equalsIgnoreCase("")) {
					/*
					 * if (payment.getPayFullButton().isDisplayed()||payment.getPartPayButton().
					 * isDisplayed()||payment.getPayAtHotel().isDisplayed())
					 * 
					 * 
					 * payment.getPayFullButton().click();
					 */
				} else {
				}

				util.swipeDownById(driver, By.id("com.cleartrip.android:id/credit_cvv"));

			}
		}

		else if (CommonUtil.getCommonData("newdesignorold").equalsIgnoreCase("newflow")) {
			// util.swipeDownById(driver, By.id("com.cleartrip.android:id/signInTxt"));
			PaymentPageObjects payment = new PaymentPageObjects(driver);
			if (type.equalsIgnoreCase("fullamount")) {
				payment.getPayFullButton().click();

			}
			if (type.equalsIgnoreCase("partpay")) {
				if (payment.getPartPayButton().isDisplayed()) {
					System.out.println("Part pay option is enabled for this hotel");
				} else {
					System.out.println("Part pay option is NOT enabled for this hotel");
				}
				payment.getPartPayButton().click();
			}
			if (type.equalsIgnoreCase("pah")) {
				payment.getPayAtHotel().click();
			}
			if (type.equalsIgnoreCase("netbank")) {
				util.netBankRetry(driver);

			}

			else {
				if (type.equalsIgnoreCase("")) {
				}
			}

			util.swipeDownById(driver, By.id("com.cleartrip.android:id/btnSubmitPayment"));
			continueBookingButtonInTravellerPage.click();

		}
	}

	public void expiryScroll() throws Exception {
		int expiryMonth = Integer.parseInt("05");
		int expiryYear = Integer.parseInt("2021");
		final String attern = "dd-MM-yyyy";
		String dateInString = new SimpleDateFormat(attern).format(new Date());
		// System.out.println(dateInString);
		int currentMonth = Integer.parseInt(dateInString.split("-")[1]);
		int currentYear = Integer.parseInt(dateInString.split("-")[2]);
		if (currentMonth > expiryMonth) {
			while (currentMonth != expiryMonth) {
				// System.out.println(currentMonth+" "+expiryMonth);
				shortSwipeDown(driver, 0.05);
				Thread.sleep(2000);
				currentMonth--;
			}
		} else {
			while (currentMonth != expiryMonth) {
				System.out.println("curmon:" + currentMonth);
				shortSwipe1(driver, 0.8);
				Thread.sleep(2000);
				currentMonth++;
			}
		}
		while (currentYear != expiryYear) {
			shortSwipe1(driver, 1.25);
			Thread.sleep(2000);
			currentYear++;
		}
	}

	public void shortSwipe1(AppiumDriver driver, double diffX) {
		Dimension windowSize = driver.manage().window().getSize();
		int height = windowSize.height;
		System.out.println("Height  of the screen is :::::" + height);
		int width = windowSize.width;
		System.out.println("Width of the screen is :::::" + width);
		int startY = (int) (height * 0.60);
		int endY = (int) (height * 0.55);
		int startX = (int) (width / 2);
		TouchAction action = new TouchAction(driver);
		try {
			int j = (int) (startX * diffX);
			// driver.swipe(j, startY, j, endY, 800);
			action.press(j, startY).waitAction(Duration.ofMillis(500)).moveTo(j, endY).release().perform();
			// action.press(j,
			// startY).waitAction(Duration.ofMillis(500)).release().perform();
		} catch (Exception e) {
			System.out.println("Unable to do short swipe");
		}

	}

	public void shortSwipeDown(AppiumDriver driver, double diffX) {
		Dimension windowSize = driver.manage().window().getSize();
		int height = windowSize.height;
		int width = windowSize.width;
		int startY = (int) (height * 0.60);
		int endY = (int) (height * 0.50);
		int startX = (int) (width / 2);
		TouchAction action = new TouchAction(driver);

		try {
			int j = (int) (startX * diffX);
			// driver.swipe(j, endY, j, startY, 800);
			action.press(j, endY).waitAction(Duration.ofMillis(500)).moveTo(j, startY).release().perform();
		} catch (Exception e) {
			System.out.println("Unable to do short swipe");
		}

	}

	public void travelerFillForSignin() throws InterruptedException {
		CommonUtil util = new CommonUtil();
		util.waitForElement(driver, By.id("com.cleartrip.android:id/pickTraveller"));
		getPickTraveler().click();
		getAutoFillTraveler().click();

		util.swipeDownById(driver, By.id("com.cleartrip.android:id/txtTotal"));
	}

}
