package com.cleartrip.android.objects.hotel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.common.PageObject;

import io.appium.java_client.AppiumDriver;

public class HotelDetailsPageObjects extends PageObject {

	public HotelDetailsPageObjects(AppiumDriver driver) {
		super(driver);
		
	}
	
	@FindBy(id="com.cleartrip.android:id/btn_pick_rooms")
	private WebElement selectRoomButton;
	
	@FindBy(id="com.cleartrip.android:id/shorlistImage")
	private WebElement shortListImageIcon;
	
	@FindBy(id="com.cleartrip.android:id/txttaRatings")
	private WebElement taReviews;
	
	@FindBy(id="com.cleartrip.android:id/lytRemainingAmenities")
	private WebElement extraAmenities;
	
	@FindBy(id="com.cleartrip.android:id/txtCloseDialog")
	private WebElement closeExtraAmenitiesTab;
	
	@FindBy(id="com.cleartrip.android:id/tv_shortlist_text")
	private WebElement shortlistIcon;
	
	@FindBy(id="com.cleartrip.android:id/shortlistCount")
	private WebElement shortlistCount;
	
	@FindBy(id="com.cleartrip.android:id/btnCheckAvailibility")
	private WebElement checkAvailability;
	
	@FindBy(id="com.cleartrip.android:id/done")
	private WebElement doneButton;
	
	@FindBy(id="com.cleartrip.android:id/txtTravelDatesHeader")
	private WebElement travelDatesInDetailsPage;
	
	@FindBy(id="com.cleartrip.android:id/lytCheckInDate")
	private WebElement checkInDateInDetailsPage;
	
	@FindBy(id="com.cleartrip.android:id/lytCheckOutDate")
	private WebElement checkInDateOutDetailsPage;
	
	
	
	public WebElement getTravelDatesInDetailsPage() {
		return travelDatesInDetailsPage;
	}


	public WebElement getCheckInDateInDetailsPage() {
		return checkInDateInDetailsPage;
	}


	public WebElement getCheckInDateOutDetailsPage() {
		return checkInDateOutDetailsPage;
	}


	public WebElement getDoneButton() {
		return doneButton;
	}


	public WebElement getCheckAvailability() {
		return checkAvailability;
	}


	public WebElement getShortlistIcon() {
		return shortlistIcon;
	}


	public WebElement getShortlistCount() {
		return shortlistCount;
	}

	public WebElement getSelectRoomButton() {
		return selectRoomButton;
	}

	
	public WebElement getShortListImageIcon() {
		return shortListImageIcon;
	}

	public WebElement getTaReviews() {
		return taReviews;
	}

	public WebElement getExtraAmenities() {
		return extraAmenities;
	}

	public WebElement getCloseExtraAmenitiesTab() {
		return closeExtraAmenitiesTab;
	}
	
	public void selectRoomButtonInDetailsPage() {
		CommonUtil cmutil= new CommonUtil();
		cmutil.waitForElement(driver, By.id("com.cleartrip.android:id/btn_pick_rooms"));
		getSelectRoomButton().click();
		
	}
	
	public void shortlistHotelAndCheckCount() {
		CommonUtil util =new CommonUtil();
		util.waitForElement(driver, By.id("com.cleartrip.android:id/tv_shortlist_text"));
		getShortlistIcon().click();
		String count = getShortlistCount().getText();
		System.out.println("count of the shortlisted hotel is ::"+count);
		if(!(count==null)) {
			System.out.println("Hotel shortlisted from details page");
		}else {
			System.out.println("Hotel not shortlised");
		}
		
	}
	
	public void dateChangeInDetailsPage(AppiumDriver driver) throws InterruptedException {
		CommonUtil util =new CommonUtil();
		HotelHomePageObjects home = new HotelHomePageObjects(driver);
		Thread.sleep(2000);
		util.swipeDownById(driver, By.id("com.cleartrip.android:id/txtBook"));
		getCheckInDateInDetailsPage().click();
		util.swipeDown(driver, util.dateSelection(driver));
		home.getCheckInDateSelectionInDetailsPage().click();
		util.waitForElement(driver, By.xpath("//android.widget.TextView[@text='24']"));
		home.getCheckOutDateSelectionInDetailspage().click();
		

		
	}
	
	public void bookButtonInDetailsPage(AppiumDriver driver) throws InterruptedException {
		CommonUtil util =new CommonUtil();
		HotelHomePageObjects home = new HotelHomePageObjects(driver);
		Thread.sleep(2000);
		util.swipeDownById(driver, By.id("com.cleartrip.android:id/txtSeeAllRooms"));
		driver.findElement(By.id("com.cleartrip.android:id/txtBook")).click();
		

		
	}
	
	
	
	
}
