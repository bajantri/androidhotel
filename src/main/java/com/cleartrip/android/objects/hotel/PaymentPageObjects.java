package com.cleartrip.android.objects.hotel;

import java.awt.List;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.Set;

import javax.rmi.CORBA.Util;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.cleartrip.android.common.CommonPageObjects;
import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.common.PageObject;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.HidesKeyboard;
import io.appium.java_client.TouchAction;

public class PaymentPageObjects extends PageObject {

	public PaymentPageObjects(AppiumDriver driver) {
		super(driver);
		
	}

	
	@FindBy(id="com.cleartrip.android:id/btnCreditNormal")
	private WebElement clickOnCard;
	
	@FindBy(id="com.cleartrip.android:id/cardNo")
	private WebElement creditCard;
	
	@FindBy(id="com.cleartrip.android:id/expiry")
	private WebElement expiryMonthAndYear;
	
	@FindBy(id="com.cleartrip.android:id/cvv")
	private WebElement cvv;
	
	@FindBy(id="com.cleartrip.android:id/payment_book_button")
	private WebElement payButton;
	
	@FindBy(id="android:id/button1")
	private WebElement okButton;
	
	@FindBy(xpath="//android.widget.Button[@text='Submit']")
	private WebElement authenticationInPayment;
	
	@FindBy(id="com.cleartrip.android:id/booking_trip_id_content_trip_confirmation")
	private WebElement tripId;
	
	@FindBy(id="com.cleartrip.android:id/pay_full_amount_radio")
	private WebElement PayFullButton;
	
	@FindBy(id="com.cleartrip.android:id/etxtCouponcode")
	private WebElement couponcodeField;
	
	@FindBy(id="com.cleartrip.android:id/check_savings")
	private WebElement checkSavingsButton;
	
	@FindBy(id="com.cleartrip.android:id/part_pay_radio")
	private WebElement partPayButton;
	
	@FindBy(id="com.cleartrip.android:id/pay_at_hotel_radio")
	private WebElement payAtHotel;
	
	@FindBy(xpath="//android.widget.Button[@text='Cancel']")
	private WebElement paytmCancelButton;
	
	@FindBy(id="android:id/button1")
	private WebElement paytmRetryPopUp;
	
	@FindBy(id="com.cleartrip.android:id/btnNetbankingNormal")
	private WebElement netBankingLink;
	
	@FindBy(id="com.cleartrip.android:id/lytNetBanking")
	private WebElement selectBankDropDown;
	
	@FindBy(xpath="//android.widget.TextView[@text='State Bank of India']")
	private WebElement selectBank;
	
	@FindBy(id="com.cleartrip.android:id/fareBreakUp")
	private WebElement fareBreakUpForPahcc;
	
	@FindBy(xpath="//android.widget.TextView[@text='Net Banking']")
	private WebElement netBankingDropDownButton;
	
	@FindBy(id="com.cleartrip.android:id/radioBtn")
	private java.util.List<WebElement> selectBankFromList;
	
	@FindBy(xpath="//android.widget.TextView[@text='Credit/Debit Card']")
	private WebElement creditCardDropDownInNewPage;
	

	public WebElement getNetBankingDropDownButton() {
		return netBankingDropDownButton;
	}

	public WebElement getCreditCardDropDownInNewPage() {
		return creditCardDropDownInNewPage;
	}

	
	
	public java.util.List<WebElement> getSelectBankFromList() {
		return selectBankFromList;
	}

	public WebElement getFareBreakUpForPahcc() {
		return fareBreakUpForPahcc;
	}

	public WebElement getNetBankingLink() {
		return netBankingLink;
	}

	public WebElement getSelectBankDropDown() {
		return selectBankDropDown;
	}

	public WebElement getSelectBank() {
		return selectBank;
	}

	public WebElement getPaytmRetryPopUp() {
		return paytmRetryPopUp;
	}

	public WebElement getPaytmCancelButton() {
		return paytmCancelButton;
	}

	public WebElement getAuthenticationInPayment() {
		return authenticationInPayment;
	}

	public WebElement getCouponcodeField() {
		return couponcodeField;
	}

	public WebElement getCheckSavingsButton() {
		return checkSavingsButton;
	}

	public WebElement getPayFullButton() {
		return PayFullButton;
	}

	public WebElement getPartPayButton() {
		return partPayButton;
	}

	public WebElement getPayAtHotel() {
		return payAtHotel;
	}
	
	
	public WebElement getTripId() {
		System.out.println("TRIP ID :: "+tripId.getText());
		return tripId;
	}

	public WebElement getAuthenticationInPayment(AppiumDriver driver) throws InterruptedException {
		CommonUtil cm=new CommonUtil();
		//cm.waitForElement(driver, By.xpath("//android.widget.Button[@text='Submit']"));
		Thread.sleep(30000);
		if(authenticationInPayment.isDisplayed()) {
			authenticationInPayment.click();
			//driver.findElement(By.xpath("//android.widget.Button[@text='Submit']")).click();
			//authenticationInPayment.click();
			System.out.println("Submit button clicked :::::");
		}else {
			System.out.println("Page not loaded ::::::::::::");
		}
		return authenticationInPayment;
	}

	public WebElement getOkButton() {
		return okButton;
	}

	public WebElement getPayButton() {
		return payButton;
	}

	public WebElement getClickOnCard() {
		return clickOnCard;
	}

	public WebElement getCreditCard() {
		creditCard.sendKeys("5123456789012346");
		return creditCard;
	}

	public WebElement getExpiryMonthAndYear() throws Exception {
		expiryMonthAndYear.click();
		return expiryMonthAndYear;
	}

	public WebElement getCvv() {
		cvv.sendKeys("123");
		return cvv;
	}
	
	public void expiryScroll() throws Exception  {
		int expiryMonth=Integer.parseInt("05");
		int expiryYear=Integer.parseInt("2021");
		 final String attern = "dd-MM-yyyy";
		  String dateInString =new SimpleDateFormat(attern).format(new Date());
		 // System.out.println(dateInString);
		  int currentMonth=Integer.parseInt(dateInString.split("-")[1]);
		  int currentYear=Integer.parseInt(dateInString.split("-")[2]);
		  if(currentMonth>expiryMonth){
			  while(currentMonth!=expiryMonth){
				 // System.out.println(currentMonth+" "+expiryMonth);
				  //shortSwipeDown(driver,0.05);
				  Thread.sleep(2000);
				  currentMonth--;
			  }
		  }
		  else{
			  while(currentMonth!=expiryMonth){
				  System.out.println("curmon:"+currentMonth);
				  shortSwipe1(driver,0.8);
				  Thread.sleep(2000);
				  currentMonth++;
			  }
		  }
			  while(currentYear!=expiryYear){
				  shortSwipe1(driver,1.25);
				  Thread.sleep(2000);
				  currentYear++;
		    }
	}
	
	public void shortSwipe1(AppiumDriver driver, double diffX) {
		Dimension windowSize = driver.manage().window().getSize();
		int height = windowSize.height;
		System.out.println("Height  of the screen is :::::"+ height);
		int width = windowSize.width;
		System.out.println("Width of the screen is :::::"+ width);
		int startY = (int) (height * 0.60);
		int endY = (int) (height * 0.55);
		int startX = (int) (width / 2);
		TouchAction action = new TouchAction(driver);
		try {
			int j = (int) (startX * diffX);
			//driver.swipe(j, startY, j, endY, 800);
			action.press(j, startY).waitAction(Duration.ofMillis(500)).moveTo(j,endY ).release().perform();
			//action.press(j, startY).waitAction(Duration.ofMillis(500)).release().perform();
		} catch (Exception e) {
			System.out.println("Unable to do short swipe");
		}

	} 
	
	public void creditCardPayment() throws Exception {
		
		getCreditCard();
		driver.hideKeyboard();
		getExpiryMonthAndYear();
		CommonUtil util = new CommonUtil();
		util.expiryScroll(driver);
		CommonPageObjects cp = new CommonPageObjects(driver);
		cp.getExpiryDialogueOkButton().click();
		getCvv();
		driver.hideKeyboard();
		getPayButton().click();
		System.out.println("payment button click done");
	}
	
	public void paytmRetry() {
		CommonUtil util = new CommonUtil();
		util.waitForElement(driver, By.xpath("//android.widget.Button[@text='Cancel']"));
		getPaytmCancelButton().click();
		getPaytmRetryPopUp().click();
	}

	public void pahccCreditCardPaymentInNewPage(AppiumDriver driver) throws Exception {
		CommonUtil util = new CommonUtil();
		util.swipeDownById(driver, By.id("com.cleartrip.android:id/fareBreakUp"));
		util.makePayment(driver, "creditcard", "mastercard", "");
	}
		
	
	

	
}
