package com.cleartrip.android.objects.hotel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.common.PageObject;

import io.appium.java_client.AppiumDriver;

public class RoomSelectionPageObjects extends PageObject{

	public RoomSelectionPageObjects(AppiumDriver driver) {
		super(driver);
		
	}
	
	@FindBy(id="com.cleartrip.android:id/btn_pick_rooms")
	private WebElement continueBookingButton;
	
	@FindBy(id="com.cleartrip.android:id/txtBook")
	private WebElement bookButtonInNewDetailsPage;

	public WebElement getBookButtonInNewDetailsPage() {
		return bookButtonInNewDetailsPage;
	}


	public WebElement getContinueBookingButton() {
		return continueBookingButton;
	}
	
	
	public void bookButton(AppiumDriver driver ) throws InterruptedException {
		String oldOrNewFlow = "newflow";
		if(oldOrNewFlow.equalsIgnoreCase("oldflow")) {
			CommonUtil util=new CommonUtil();
			util.waitForElement(driver, By.id("com.cleartrip.android:id/btn_pick_rooms"));
			getContinueBookingButton().click();
		}
		else if (oldOrNewFlow.equalsIgnoreCase("newflow")){
			CommonUtil util=new CommonUtil();
			boolean a = util.waitForElement(driver, By.id("com.cleartrip.android:id/txtBook"));
			getBookButtonInNewDetailsPage().click();
			
		}
		/*Thread.sleep(3000);
		 if(driver.findElement(By.id("com.cleartrip.android:id/btn_pick_rooms")).isDisplayed()){
		
			getContinueBookingButton().click();
		}
		else if  ( driver.findElement(By.id("com.cleartrip.android:id/txtBook")).isDisplayed()) {
			
			
				getBookButtonInNewDetailsPage().click();
			}
		}
		*/
	}
	
}

