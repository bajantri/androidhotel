package com.cleartrip.android.objects.local.common;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.cleartrip.android.common.Base;
import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.common.DeviceManager;
import com.cleartrip.android.common.PageObject;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.appium.java_client.AppiumDriver;

public class ItineraryPageObjects extends PageObject {

	public ItineraryPageObjects(AppiumDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	private ObjectMapper mapper = new ObjectMapper();
	public String androidOs = "7.1.1";

	@FindBy(id = "com.cleartrip.android:id/backBtn")
	private WebElement backButton;

	@FindBy(id = "com.cleartrip.android:id/lcl_vbf_header_title")
	private WebElement varientHeader;

	@FindBy(id = "com.cleartrip.android:id/lcl_vbf_header_sub_title")
	private WebElement varientArea;

	@FindBy(id = "com.cleartrip.android:id/date")
	private WebElement dateTime;

	@FindBy(id = "com.cleartrip.android:id/duration")
	private WebElement totalDuration;

	@FindBy(id = "com.cleartrip.android:id/value")
	private WebElement totalPeople;

	@FindBy(id = "com.cleartrip.android:id/price")
	private WebElement basePrice;

	@FindBy(id = "com.cleartrip.android:id/totalPrice")
	private WebElement totalPrice;

	@FindBy(id = "com.cleartrip.android:id/lcl_vbf_booking_policy")
	private WebElement cancelPolicy;

	@FindBy(id = "com.cleartrip.android:id/mrRb")
	private WebElement mrTab;

	@FindBy(id = "com.cleartrip.android:id/msRb")
	private WebElement msTab;

	@FindBy(id = "com.cleartrip.android:id/mrsRb")
	private WebElement mrsTab;

	@FindBy(id = "com.cleartrip.android:id/nameTxt")
	private WebElement nameTextBox;
	
	@FindBy(id = "com.cleartrip.android:id/lastNameTxt")
	private WebElement lastNameTxt;

	@FindBy(id = "com.cleartrip.android:id/emailTxt")
	private WebElement emailTextBox;

	@FindBy(id = "com.cleartrip.android:id/input_layout_phone")
	//com.cleartrip.android:id/input_layout_phone
	//com.cleartrip.android:id/phoneTxt
	private WebElement phoneTextBox;

	@FindBy(id = "com.cleartrip.android:id/txtUseGst")
	private WebElement gstTextBox;

	@FindBy(id = "com.cleartrip.android:id/stateLayout")
	private WebElement stateTextBox;

	@FindBy(id = "com.cleartrip.android:id/etxtCouponcode")
	private WebElement couponCodeTextBox;

	@FindBy(id = "com.cleartrip.android:id/check_savings")
	private WebElement checkSavings;

	@FindBy(id = "com.cleartrip.android:id/etxtGiftPin")
	private WebElement giftPin;

	@FindBy(id = "com.cleartrip.android:id/signInTxt")
	private WebElement signIn;

	@FindBy(id = "com.cleartrip.android:id/btnCreditNormal")
	private WebElement cardsTab;

	@FindBy(id = "com.cleartrip.android:id/btnNetbankingNormal")
	private WebElement netBankingtab;

	@FindBy(id = "com.cleartrip.android:id/bankWallet")
	private WebElement walletTab;

	//@FindBy(xpath = "//android.widget.RadioButton[contains(@text,'Card')]")
	//private WebElement cardNumberTextBox;
	
	@FindBy(id = "com.cleartrip.android:id/cardNo")
	//com.cleartrip.android:id/card_number_wrapper
	//com.cleartrip.android:id/card_number
	private WebElement cardNumberTextBox;

	@FindBy(id = "com.cleartrip.android:id/expiry")
	private WebElement exipreDateTextBox;

	@FindBy(id = "com.cleartrip.android:id/cvv")
	private WebElement cvvTextBox;

	@FindBy(id = "com.cleartrip.android:id/monthLyt")
	private WebElement monthLayout;

	@FindBy(id = "com.cleartrip.android:id/yearLyt")
	private WebElement yearLayout;

	@FindBy(id = "android:id/button1")
	private WebElement expOkButton;

	@FindBy(id = "android:id/button2")
	private WebElement expCancelButton;

	@FindBy(id = "com.cleartrip.android:id/btnSubmitPayment")
	private WebElement paySecurelyButton;
	
	@FindBy(id = "com.cleartrip.android:id/payment_book_button")
	private WebElement payNowButton;

	@FindBy(id = "com.cleartrip.android:id/lytNetBanking")
	private WebElement selectBankdropdown;

	@FindBy(id = "com.cleartrip.android:id/radioBtnBankWallet")
	private WebElement radioButoon;

	@FindBy(id = "com.cleartrip.android:id/walletImage")
	private WebElement walletName;

	@FindBy(id = "com.cleartrip.android:id/txtTotal")
	private WebElement totalAmountText;

	@FindBy(id = "com.cleartrip.android:id/txt_Money")
	private WebElement totalMoney;

	@FindBy(id = "com.cleartrip.android:id/terms_and_condition")
	private WebElement linkTermsandCondition;

	@FindBy(xpath = "//android.widget.Button[@text='Submit']")
	private WebElement submitButton;

	public WebElement getBackButton() {
		return backButton;
	}

	public WebElement getVarientHeader() {
		return varientHeader;
	}

	public WebElement getVarientArea() {
		return varientArea;
	}

	public WebElement getDateTime() {
		return dateTime;
	}

	public WebElement getTotalDuration() {
		return totalDuration;
	}

	public WebElement getTotalPeople() {
		return totalPeople;
	}

	public WebElement getBasePrice() {
		return basePrice;
	}

	public WebElement getTotalPrice() {
		return totalPrice;
	}

	public WebElement getCancelPolicy() {
		return cancelPolicy;
	}

	public WebElement getMrTab() {
		return mrTab;
	}

	public WebElement getMsTab() {
		return msTab;
	}

	public WebElement getMrsTab() {
		return mrsTab;
	}

	public WebElement getNameTextBox() {
		return nameTextBox;
	}
	
	public WebElement getlastNameTxt() {
		return lastNameTxt;
	}

	public WebElement getEmailTextBox() {
		return emailTextBox;
	}

	public WebElement getPhoneTextBox() {
		return phoneTextBox;
	}

	public WebElement getGstTextBox() {
		return gstTextBox;
	}

	public WebElement getStateTextBox() {
		return stateTextBox;
	}

	public WebElement getCouponCodeTextBox() {
		return couponCodeTextBox;
	}

	public WebElement getCheckSavings() {
		return checkSavings;
	}

	public WebElement getGiftPin() {
		return giftPin;
	}

	public WebElement getSignIn() {
		return signIn;
	}

	public WebElement getCardsTab() {
		return cardsTab;
	}

	public WebElement getNetBankingtab() {
		return netBankingtab;
	}

	public WebElement getWalletTab() {
		return walletTab;
	}

	public WebElement getCardNumberTextBox() {
		return cardNumberTextBox;
	}

	public WebElement getExipreDateTextBox() {
		return exipreDateTextBox;
	}

	public WebElement getCvvTextBox() {
		return cvvTextBox;
	}

	public WebElement getMonthLayout() {
		return monthLayout;
	}

	public WebElement getYearLayout() {
		return yearLayout;
	}

	public WebElement getExpOkButton() {
		return expOkButton;
	}

	public WebElement getExpCancelButton() {
		return expCancelButton;
	}

	public WebElement getPaySecurelyButton() {
		return paySecurelyButton;
	}
	
	public WebElement getpayNowButton() {
		return payNowButton;
	}

	public WebElement getSelectBankdropdown() {
		return selectBankdropdown;
	}

	public WebElement getRadioButoon() {
		return radioButoon;
	}

	public WebElement getWalletName() {
		return walletName;
	}

	public WebElement getTotalAmountText() {
		return totalAmountText;
	}

	public WebElement getTotalMoney() {
		return totalMoney;
	}

	public WebElement getLinkTermsandCondition() {
		return linkTermsandCondition;
	}

	public WebElement getSubmitButton() {
		return submitButton;
	}

	public void fillTravellerDetailsSignedIn(AppiumDriver<WebElement> driver) throws InterruptedException {
		CommonUtil util = new CommonUtil();
		util.waitForElement(driver, By.id("com.cleartrip.android:id/date"));
		util.swipeDownById(driver, By.id("com.cleartrip.android:id/phoneTxt"));
		Thread.sleep(2000);
		getNameTextBox().sendKeys("jitu");
		driver.hideKeyboard();
		
		getlastNameTxt().sendKeys("jitu");
		driver.hideKeyboard();
		
	/*	
		getEmailTextBox().click();
		getEmailTextBox().sendKeys("Jitu@gmail.com");
		driver.hideKeyboard();*/
		
	   /* getPhoneTextBox().click();
	    getPhoneTextBox().sendKeys("9663806628");
		driver.hideKeyboard();
*/
	}
	
	public void fillTravellerDetails(AppiumDriver<WebElement> driver) throws InterruptedException {
		CommonUtil util = new CommonUtil();
		util.waitForElement(driver, By.id("com.cleartrip.android:id/date"));
		util.swipeDownById(driver, By.id("com.cleartrip.android:id/phoneTxt"));
		
		//util.swipeDownById(driver, By.id("com.cleartrip.android:id/phoneTxt"));
		getNameTextBox().sendKeys("Jitu");
		driver.hideKeyboard();
		getlastNameTxt().sendKeys("Jitu");
		driver.hideKeyboard();
		getEmailTextBox().click();
		getEmailTextBox().sendKeys("Jitu@gmail.com");
		driver.hideKeyboard();
	    getPhoneTextBox().click();
	    getPhoneTextBox().sendKeys("9663806628");
		driver.hideKeyboard();

	}

	public void fillCardDetail(AppiumDriver<WebElement> driver) throws Exception {
		CommonUtil util = new CommonUtil();
		util.makePayment(driver, "creditcard", "mastercard", "");
		//util.swipeDownById(driver, By.id("com.cleartrip.android:id/credit_cvv"));
	/*	getCardNumberTextBox().click();
		getCardNumberTextBox().sendKeys("5123456789012346");
		driver.hideKeyboard();
		getExipreDateTextBox().click();
		
		 getExpOkButton().click();
		getCvvTextBox().click();
		getCvvTextBox().sendKeys("123");
		driver.hideKeyboard();*/

	}

	public void fillCardDetailSignedIn(AppiumDriver<WebElement> driver) throws Exception {
		CommonUtil util = new CommonUtil();
		util.makePayment(driver, "creditcard", "mastercard", "");
		
		//util.swipeDownById(driver, By.id("com.cleartrip.android:id/ctActionBar"));
	/*	getCardNumberTextBox().click();
		getCardNumberTextBox().sendKeys("5123456789012346");
		driver.hideKeyboard();
		getExipreDateTextBox().click();
		
		
			getExpOkButton().click();
			getCvvTextBox().click();
			getCvvTextBox().sendKeys("123");
			driver.hideKeyboard();
*/
	}
		  	
	public void makePayment(AppiumDriver<WebElement> driver) throws InterruptedException {
		CommonUtil util = new CommonUtil();
		util.swipeDownById(driver, By.id("com.cleartrip.android:id/btnSubmitPayment"));
		getPaySecurelyButton().click();
		//util.waitForElement(driver, By.xpath("//android.widget.Button[@text='Submit']"));
		//util.swipeDownById(driver, By.id("selectAuthResult"));
		//getSubmitButton().click();

	}
	
	public void payNowButton(AppiumDriver<WebElement> driver) throws InterruptedException {
		CommonUtil util = new CommonUtil();
		getpayNowButton().click();
		util.waitForElement(driver, By.xpath("//android.widget.Button[@text='Submit']"));
		//util.swipeDownById(driver, By.id("selectAuthResult"));
		getSubmitButton().click();

	}
	
	
	public void applyCoupon(AppiumDriver<WebElement> driver, String coupon) 
			throws InterruptedException, IOException  {
		
		CommonUtil util = new CommonUtil();
		util.swipeDownById(driver, By.id("com.cleartrip.android:id/check_savings"));
		
		FileReader reader=new FileReader("D://Androd_Automation_new/resources/common.properties");  
	    Properties p=new Properties();  
	    p.load(reader);  	    
		getCouponCodeTextBox().click();
		couponCodeTextBox.sendKeys(p.getProperty(coupon));
		driver.hideKeyboard();
		//driver.navigate().back();
		getCheckSavings().click();
		util.swipeDownById(driver, By.id("com.cleartrip.android:id/btnSubmitPayment"));
		//util.waitForElement(driver, By.id("com.cleartrip.android:id/cpn_message"));
		System.out.println("Coupon code sucessfully applied");
				
	}
	
}
