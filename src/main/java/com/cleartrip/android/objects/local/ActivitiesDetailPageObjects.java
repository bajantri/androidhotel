package com.cleartrip.android.objects.local;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.common.PageObject;

import io.appium.java_client.AppiumDriver;

public class ActivitiesDetailPageObjects extends PageObject {

	public ActivitiesDetailPageObjects(AppiumDriver driver) {
		super(driver);
	}

	@FindBy(id = "com.cleartrip.android:id/imageView")
	private WebElement detailPageImage;

	@FindBy(id = "com.cleartrip.android:id/txt_discount")
	private WebElement discountText;

	@FindBy(id = "com.cleartrip.android:id/txt_strike_price")
	private WebElement strikePrice;

	@FindBy(id = "com.cleartrip.android:id/txt_price")
	private WebElement price;

	@FindBy(id = "com.cleartrip.android:id/ltda_bread_crumb")
	private WebElement breadCrumb;

	@FindBy(id = "com.cleartrip.android:id/ltda_title")
	private WebElement activityName;

	@FindBy(id = "com.cleartrip.android:id/ltda_supplier_label")
	private WebElement ratingReviews;

	@FindBy(id = "com.cleartrip.android:id/avg_rating")
	private WebElement avgRating;

	@FindBy(id = "com.cleartrip.android:id/ltda_about_label")
	private WebElement whyThisExp;

	@FindBy(xpath = "//android.widget.TextView[@text='READ MORE']")
	private WebElement readMore;

	@FindBy(id = "com.cleartrip.android:id/cti_text")
	private WebElement varientTab;

	@FindBy(id = "com.cleartrip.android:id/ltda_inclusion_label")
	private WebElement inclusionSection;

	@FindBy(id = "com.cleartrip.android:id/ltda_price_label")
	private WebElement priceSection;

	@FindBy(id = "com.cleartrip.android:id/ltda_time_label")
	private WebElement whenSection;

	@FindBy(id = "com.cleartrip.android:id/ltda_supplier_label")
	private WebElement organisedBySection;

	@FindBy(id = "com.cleartrip.android:id/ltda_address_label")
	private WebElement whereSection;

	@FindBy(id = "com.cleartrip.android:id/ltda_bottom_image_view")
	private WebElement detailBottomImage;

	public WebElement getBreadCrumb() {
		return breadCrumb;
	}

	public WebElement getActivityName() {
		return activityName;
	}

	public WebElement getRatingReviews() {
		return ratingReviews;
	}

	public WebElement getAvgRating() {
		return avgRating;
	}

	public WebElement getWhyThisExp() {
		return whyThisExp;
	}

	public WebElement getReadMore() {
		return readMore;
	}

	public WebElement getVarientTab() {
		return varientTab;
	}

	public WebElement getInclusionSection() {
		return inclusionSection;
	}

	public WebElement getPriceSection() {
		return priceSection;
	}

	public WebElement getWhenSection() {
		return whenSection;
	}

	public WebElement getOrganisedBySection() {
		return organisedBySection;
	}

	public WebElement getWhereSection() {
		return whereSection;
	}

	public WebElement getDetailBottomImage() {
		return detailBottomImage;
	}

	public WebElement getDirectionIcon() {
		return directionIcon;
	}

	public WebElement getBottomReadMore() {
		return bottomReadMore;
	}

	public WebElement getBookNow() {
		return bookNow;
	}
	
	public WebElement getBook() {
		return book;
	}

	@FindBy(id = "com.cleartrip.android:id/ltda_direction_icon")
	private WebElement directionIcon;

	@FindBy(id = "com.cleartrip.android:id/read_more")
	private WebElement bottomReadMore;

	@FindBy(id = "com.cleartrip.android:id/bookbutton")
	private WebElement bookNow;
	
	@FindBy(id = "com.cleartrip.android:id/b_package_booknow")
	private WebElement book;

	public WebElement getDetailPageImage() {
		return detailPageImage;
	}

	public WebElement getDiscountText() {
		return discountText;
	}

	public WebElement getStrikePrice() {
		return strikePrice;
	}

	public WebElement getPrice() {
		return price;
	}
		
		public void clickBookNowButton(AppiumDriver driver) {
		
			CommonUtil util = new CommonUtil();
			util.waitForElement(driver, By.id("com.cleartrip.android:id/bookbutton"));
			getBookNow().click();
			//getBook().click();
			
			
		}
		
}
