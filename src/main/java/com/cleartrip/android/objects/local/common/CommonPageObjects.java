package com.cleartrip.android.objects.local.common;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CommonPageObjects {

	@FindBy(id = "com.cleartrip.android:id/city_spinner")
	private WebElement citySelect;

	@FindBy(id = "com.cleartrip.android:id/filter_edittext")
	private WebElement typeCity;

	@FindBy(xpath = "//android.widget.TextView[@text='DEALS']")
	private WebElement deals;

	@FindBy(id = "com.cleartrip.android:id/lcl_wishlist")
	private WebElement wishlistIcon;

	@FindBy(id = "com.cleartrip.android:id/shareImg")
	private WebElement shareIcon;

	@FindBy(id = "com.cleartrip.android:id/lclSearchImage")
	private WebElement searchIcon;

	@FindBy(className = "android.widget.ImageButton")
	private WebElement backbutton;

	@FindBy(id = "com.cleartrip.android:id/sortTxt")
	private WebElement sortbutton;

	@FindBy(id = "com.cleartrip.android:id/distanceTxt")
	private WebElement selectByDistance;

	@FindBy(id = "com.cleartrip.android:id/priceTxt")
	private WebElement selectByPrice;

	@FindBy(id = "com.cleartrip.android:id/popularityTxt")
	private WebElement selectByPopularity;

	@FindBy(id = "com.cleartrip.android:id/ltda_shortlist_button")
	private WebElement detailShortList;

	@FindBy(id = "com.cleartrip.android:id/ltda_share_button")
	private WebElement detailShareButton;

	public WebElement getcitySelect() {
		return citySelect;
	}

	public WebElement getTypeCity() {
		return typeCity;
	}

	public WebElement getdeals() {
		return deals;
	}

	public WebElement getwishListIcon() {
		return wishlistIcon;
	}

	public WebElement getshareIcon() {
		return shareIcon;
	}

	public WebElement getsearchIcon() {
		return searchIcon;
	}

	public WebElement getbackButton() {
		return backbutton;
	}

	public WebElement getsortbutton() {
		return sortbutton;
	}

	public WebElement getselectByDistance() {
		return selectByDistance;
	}

	public WebElement getselectByPrice() {
		return selectByPrice;
	}

	public WebElement getselectByPopularity() {
		return selectByPopularity;
	}

	public WebElement getdetailShortList() {
		return detailShortList;
	}

	public WebElement getdetailShareButton() {
		return detailShareButton;
	}

}
