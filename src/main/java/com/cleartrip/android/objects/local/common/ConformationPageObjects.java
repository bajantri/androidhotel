package com.cleartrip.android.objects.local.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.common.PageObject;

import io.appium.java_client.AppiumDriver;

public class ConformationPageObjects extends PageObject  {

	public ConformationPageObjects(AppiumDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@FindBy(xpath = "//android.widget.Button[@text='Submit']")
	private WebElement readMore;

	@FindBy(id = "com.cleartrip.android:id/booking_price_trip_confirmation")
	private WebElement conformationPrice;

	@FindBy(id = "com.cleartrip.android:id/booking_header_trip_confirmation")
	private WebElement bookingDoneText;

	@FindBy(id = "com.cleartrip.android:id/booking_payment_mode_trip_confirmation")
	private WebElement paymentThrough;

	@FindBy(id = "com.cleartrip.android:id/booking_trip_id_content_trip_confirmation")
	private WebElement tripId;

	@FindBy(id = "com.cleartrip.android:id/shareBtn")
	private WebElement shareButton;

	@FindBy(id = "com.cleartrip.android:id/view_trip_details_trip_confirmation")
	private WebElement viewTripDetails;

	public WebElement getReadMore() {
		return readMore;
	}

	public WebElement getConformationPrice() {
		return conformationPrice;
	}

	public WebElement getBookingDoneText() {
		return bookingDoneText;
	}

	public WebElement getPaymentThrough() {
		return paymentThrough;
	}

	public WebElement getTripId() {
		return tripId;
	}

	public WebElement getShareButton() {
		return shareButton;
	}

	public WebElement getViewTripDetails() {
		return viewTripDetails;
	}

	
	public void clickViewTripDetail(AppiumDriver<WebElement>  driver) throws InterruptedException {
		CommonUtil util = new CommonUtil();
		util.waitForElement(driver, By.id("com.cleartrip.android:id/view_trip_details_trip_confirmation"));
		getViewTripDetails().click();
			
	}
}
