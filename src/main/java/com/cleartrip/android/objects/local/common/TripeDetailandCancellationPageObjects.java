package com.cleartrip.android.objects.local.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.common.PageObject;

import io.appium.java_client.AppiumDriver;

public class TripeDetailandCancellationPageObjects extends PageObject  {
	public TripeDetailandCancellationPageObjects(AppiumDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@FindBy(className = "android.widget.ImageButton")
	private WebElement backButton;

	@FindBy(id = "com.cleartrip.android:id/trip_name_center")
	private WebElement activityNameBooked;

	@FindBy(id = "com.cleartrip.android:id/direction")
	private WebElement getDirection;

	@FindBy(id = "com.cleartrip.android:id/btnCall")
	private WebElement callOrganiser;

	@FindBy(id = "com.cleartrip.android:id/mail")
	private WebElement shareButton;

	@FindBy(id = "com.cleartrip.android:id/btn_cancel")
	private WebElement cancelButton;

	@FindBy(id = "com.cleartrip.android:id/timeTxtview")
	private WebElement dateTimeActivityBooked;

	@FindBy(id = "com.cleartrip.android:id/durationTxtVIew")
	private WebElement totalDaysBooked;

	@FindBy(id = "com.cleartrip.android:id/tripid")
	private WebElement tripId;

	@FindBy(id = "com.cleartrip.android:id/addrDirection")
	private WebElement addressDirection;

	@FindBy(id = "com.cleartrip.android:id/buttonContactCall")
	private WebElement organiserCall;

	@FindBy(id = "com.cleartrip.android:id/txt_traveller_details")
	private WebElement bookedTravellerName;

	@FindBy(id = "com.cleartrip.android:id/txt_totalamount")
	private WebElement totalAmountBooked;

	@FindBy(id = "com.cleartrip.android:id/payemntType")
	private WebElement paymentMadeThrough;

	@FindBy(id = "com.cleartrip.android:id/btnsupport")
	private WebElement callCustomerSupport;

	@FindBy(id = "trip_cancel")
	private WebElement cancelNow;

	@FindBy(id = "noCancel")
	private WebElement dontCancelNow;

	@FindBy(xpath = "//android.view.View[@text='Back to trips']")
	private WebElement readMore;

	@FindBy(id = "com.cleartrip.android:id/trip_txtStatus_payment")
	private WebElement cancelled;

	@FindBy(id = "com.cleartrip.android:id/txt_refund")
	private WebElement refundAmount;

	@FindBy(id = "com.cleartrip.android:id/refundType")
	private WebElement refundtype;

	public WebElement getBackButton() {
		return backButton;
	}

	public WebElement getActivityNameBooked() {
		return activityNameBooked;
	}

	public WebElement getGetDirection() {
		return getDirection;
	}

	public WebElement getCallOrganiser() {
		return callOrganiser;
	}

	public WebElement getShareButton() {
		return shareButton;
	}

	public WebElement getCancelButton() {
		return cancelButton;
	}

	public WebElement getDateTimeActivityBooked() {
		return dateTimeActivityBooked;
	}

	public WebElement getTotalDaysBooked() {
		return totalDaysBooked;
	}

	public WebElement getTripId() {
		return tripId;
	}

	public WebElement getAddressDirection() {
		return addressDirection;
	}

	public WebElement getOrganiserCall() {
		return organiserCall;
	}

	public WebElement getBookedTravellerName() {
		return bookedTravellerName;
	}

	public WebElement getTotalAmountBooked() {
		return totalAmountBooked;
	}

	public WebElement getPaymentMadeThrough() {
		return paymentMadeThrough;
	}

	public WebElement getCallCustomerSupport() {
		return callCustomerSupport;
	}

	public WebElement getCancelNow() {
		return cancelNow;
	}

	public WebElement getDontCancelNow() {
		return dontCancelNow;
	}

	public WebElement getReadMore() {
		return readMore;
	}

	public WebElement getCancelled() {
		return cancelled;
	}

	public WebElement getRefundAmount() {
		return refundAmount;
	}

	public WebElement getRefundtype() {
		return refundtype;
	}

	public void getTripId(AppiumDriver<WebElement>  driver) throws InterruptedException {
		CommonUtil util = new CommonUtil();
		util.waitForElement(driver, By.id("com.cleartrip.android:id/tripid"));
		 String tripId1 = driver.findElement(By.id("com.cleartrip.android:id/tripid")).getText();
		 System.out.println(tripId1);
		 
	}
	
}
