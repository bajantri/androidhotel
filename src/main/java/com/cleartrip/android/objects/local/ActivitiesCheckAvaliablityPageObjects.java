package com.cleartrip.android.objects.local;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Reporter;

import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.common.PageObject;

import io.appium.java_client.AppiumDriver;

public class ActivitiesCheckAvaliablityPageObjects extends PageObject {

	
	public ActivitiesCheckAvaliablityPageObjects(AppiumDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@FindBy(id = "com.cleartrip.android:id/ttdHeader")
	private WebElement Header;

	@FindBy(id = "com.cleartrip.android:id/time")
	private WebElement varientTab;

	@FindBy(id = "com.cleartrip.android:id/date")
	private WebElement pickDateTime;

	@FindBy(id = "com.cleartrip.android:id/increamentBtn")
	private WebElement increamentBtn;

	@FindBy(id = "com.cleartrip.android:id/decreamentBtn")
	private WebElement decreamentBtn;

	@FindBy(id = "com.cleartrip.android:id/numberTxt")
	private WebElement numberTxt;

	@FindBy(id = "com.cleartrip.android:id/btn_book_bottom_sheet")
	private WebElement bookButton;

	@FindBy(id = "com.cleartrip.android:id/chooseDateTimeTxt")
	private WebElement checkAvaliablityHeader;
	
	public WebElement getcheckAvaliablityHeader() {
		return checkAvaliablityHeader;
	}
	
	
	public WebElement getCheckAvaliablityHeader() {
		return Header;
	}

	public WebElement getVarientTab() {
		return varientTab;
	}

	public WebElement getPickDateTime() {
		return pickDateTime;
	}

	public WebElement getIncreamentBtn() {
		return increamentBtn;
	}

	public WebElement getDecreamentBtn() {
		return decreamentBtn;
	}

	public WebElement getNumberTxt() {
		return numberTxt;
	}

	public WebElement getBookButton() {
		return bookButton;
	}

	public void datePicker(AppiumDriver driver) {

		getPickDateTime().click();

	}

	public void selectTimeSlot(AppiumDriver<WebElement> driver) throws InterruptedException {

		List<WebElement> dates = driver
				.findElements(By.xpath("//android.widget.LinearLayout[@enabled='true'][@clickable='true']"));
		
		
		if (dates.get(1).isDisplayed()) {
			dates.get(1).click();
		} else {
			Reporter.log("The date is not clickable");
		}
		List<WebElement> slotTimings = driver.findElementsByXPath("//android.widget.RadioButton[@enabled='true']");
		slotTimings.get(1).click();
	}
	
	public void selectPax(String AdultPax, String childPax, boolean isGroupActivity) {
		
		
		
	}

	public void clickBookNow(AppiumDriver driver) {

		getBookButton().click();
		
	
	}
	
	public void selectDateTime(AppiumDriver driver,String AdultPax, String childPax, boolean isGroupActivity) throws InterruptedException {
		
		CommonUtil util = new CommonUtil();
		util.waitForElement(driver, By.id("com.cleartrip.android:id/ctActionBar"));
		//datePicker(driver);
		selectTimeSlot(driver);
		clickBookNow(driver);
		
		
	}

}
