package com.cleartrip.android.objects.local;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.common.PageObject;

import io.appium.java_client.AppiumDriver;

public class ActivitiesListPageObjects extends PageObject {

	public ActivitiesListPageObjects(AppiumDriver driver) {
		super(driver);
	}

	@FindBy(id = "com.cleartrip.android:id/favourite")
	private WebElement favouriteIcon;

	@FindBy(id = "com.cleartrip.android:id/discountTextView")
	private WebElement discountTag;

	@FindBy(id = "com.cleartrip.android:id/activityName")
	private WebElement listActivityName;

	@FindBy(id = "com.cleartrip.android:id/chainTxt")
	private WebElement areaName;

	@FindBy(id = "com.cleartrip.android:id/markedPrice")
	private WebElement markedPrice;

	@FindBy(id = "com.cleartrip.android:id/activityPrice")
	private WebElement activePrice;

	@FindBy(id = "com.cleartrip.android:id/distanceTextView")
	private WebElement distance;

	public WebElement getfaouriteIcon() {
		return favouriteIcon;
	}

	public WebElement getdiscountTag() {
		return discountTag;
	}

	public WebElement getlistActivityName() {
		return listActivityName;
	}

	public WebElement getareaName() {
		return areaName;
	}

	public WebElement getmarkedPrice() {
		return markedPrice;
	}

	public WebElement getactivePrice() {
		return activePrice;
	}

	public WebElement getdistance() {
		return distance;
	}
	
	
	public void selectListPageActivity(AppiumDriver<WebElement> driver) throws InterruptedException {
		
		CommonUtil util = new CommonUtil();
		util.waitForElement(driver, By.id("com.cleartrip.android:id/activityName"));
		WebElement clickListActivity = getlistActivityName();
		clickListActivity.click();
	}

}
