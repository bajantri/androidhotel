package com.cleartrip.android.objects.local;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.common.PageObject;

import io.appium.java_client.AppiumDriver;

public class ActivitiesCollectionPageObjects extends PageObject {

	public ActivitiesCollectionPageObjects(AppiumDriver driver) {
		super(driver);
	}

	@FindBy(xpath = "//android.widget.TextView[@text='Bangalore']")
	private WebElement selectcity;
	
	public WebElement getselectcity() {
		return selectcity;
	}
	
	@FindBy(xpath = "//android.widget.TextView[@text='All Categories']")
	private WebElement categoriesTab;

	@FindBy(xpath = "//android.widget.TextView[@text='Change yourcity on the move'])")
	WebElement screenClick;

	public WebElement getScreenClick() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		WebElement waiting = wait.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//android.widget.TextView[contains(@text,'Change your')]")));
		return waiting;
	}
	
	

	public void screenClick() {
		WebElement clickScreenIcon = getScreenClick();
		clickScreenIcon.click();
	}

	public WebElement getCollectionOrEditorials(String CollectionOrEditorials) {
		return driver.findElement(By.xpath("//android.widget.TextView[@text='" + CollectionOrEditorials + "'" + "]"));
	}

	public void selectAllActivity(AppiumDriver<WebElement> driver, String AllActivity)
			throws InterruptedException {
		CommonUtil util = new CommonUtil();		
		WebElement clickAllActivity = util.swipeDown(driver, AllActivity);
		clickAllActivity.click();

	}
	
	public void selectCollectionOrEditorials(AppiumDriver<WebElement> driver, String CollectionOrEditorials)
			throws InterruptedException {
		CommonUtil util = new CommonUtil();		
		WebElement clickCollectionOrEditorials = util.swipeDown(driver, CollectionOrEditorials);
		clickCollectionOrEditorials.click();

	}
	
	public void selectCity(AppiumDriver driver) {
		
		CommonUtil util = new CommonUtil();
		util.waitForElement(driver, By.id("com.cleartrip.android:id/header"));
		getselectcity().click();
		
		
	}

}
