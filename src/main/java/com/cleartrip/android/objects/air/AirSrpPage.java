/*package com.cleartrip.android.objects.air;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.tools.ant.launch.Launcher;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.interactions.touch.Scroll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.common.PageObject;
import com.gargoylesoftware.htmlunit.ThreadedRefreshHandler;
import com.gargoylesoftware.htmlunit.javascript.host.media.webkitMediaStream;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;

public class AirSrpPage extends PageObject {

	CommonUtil common = new CommonUtil();

	public AirSrpPage(AppiumDriver driver) {
		super(driver);
	}

	@FindBy(id = "com.cleartrip.android:id/first_run_no_baggage_okay")
	private WebElement firstRunNoBaggage;

	@FindBy(id = "com.cleartrip.android:id/flight_duration")
	private WebElement pickFlilght;

	@FindBy(id = "com.cleartrip.android:id/rdbtnSortPrice")
	private WebElement priceSort;

	@FindBy(id = "com.cleartrip.android:id/rdbtnSortDuration")
	private WebElement sortDuration;

	@FindBy(id = "com.cleartrip.android:id/rdbtnSortTime")
	private WebElement sortDeparture;

	@FindBy(id = "com.cleartrip.android:id/calendar")
	private WebElement fareCalendar;

	@FindBy(id = "com.cleartrip.android:id/fabFareAlert")
	private WebElement fareAlertTab;

	@FindBy(xpath = "//android.widget.TextView[@text='FILTER']")
	private WebElement domOwFilter;

	@FindBy(xpath = "//android.widget.TextView[@text='Filter results']")
	private WebElement filterPage;

	@FindBy(xpath = "//android.widget.TextView[@text='See more filters']")
	private WebElement seeMoreFilter;

	@FindBy(xpath = "//android.widget.CheckBox[contains(@text='Non-stop')]")
	private WebElement nonStopFilter;

	@FindBy(xpath = "//android.widget.TextView[@text='Early Morning']")
	private WebElement morningFilter;

	@FindBy(xpath = "//android.widget.TextView[@text='Evening']")
	private WebElement eveningFilter;

	@FindBy(xpath = "//android.widget.TextView[@text='Non-stop flights only']")
	private WebElement nonStopFilter1;

	@FindBy(id = "com.cleartrip.android:id/layout_multiairline")
	private WebElement multiAirFilter;

	@FindBy(xpath = "//android.widget.TextView[@text='Preferred Airlines']")
	private WebElement preferredAirLine;

	@FindBy(xpath = "//android.widget.TextView[@text='SpiceJet']")
	private WebElement airLineName;

	@FindBy(id = "com.cleartrip.android:id/fliterApplyBtn")
	private WebElement filterApplyButton;

	@FindBy(id = "com.cleartrip.android:id/btnResetFilters")
	private WebElement filterResetButton;

	@FindBy(id = "com.cleartrip.android:id/booking_price_trip_confirmation")
	private WebElement confPrice;

	@FindBy(id = "com.cleartrip.android:id/booking_trip_id_content_trip_confirmation")
	private WebElement tripId;

	@FindBy(id = "com.cleartrip.android:id/view_trip_details_trip_confirmation")
	private WebElement viewTripDetail;

	@FindBy(xpath = "//android.widget.TextView[@text='Okay']")
	private WebElement okayTripDetail;

	@FindBy(id = "com.cleartrip.android:id/leg_from_header")
	private WebElement fromLegTripDetail;

	@FindBy(id = "com.cleartrip.android:id/leg_to_header")
	private WebElement toLegTripdetail;

	@FindBy(id = "com.cleartrip.android:id/txtPaymentDetails")
	private WebElement tripDetailBreakUp;

	@FindBy(id = "com.cleartrip.android:id/txtBaseFare")
	private WebElement tripDetailBaseFare;

	@FindBy(id = "com.cleartrip.android:id/txtTax")
	private WebElement tripDetailTax;

	@FindBy(id = "com.cleartrip.android:id/txtTotalPrice")
	private WebElement tripDetailTotalPrice;

	@FindBy(id = "com.cleartrip.android:id/bookBtn")
	private WebElement book_Button;

	WebDriverWait wait = new WebDriverWait(driver, 60);

	public void selectFlight(String type) {

		if (type.equalsIgnoreCase("oneway")) {

			wait.until(
					ExpectedConditions.visibilityOfElementLocated(By.id("com.cleartrip.android:id/flight_duration")));

			this.pickFlilght.click();
		}

		else if (type.equalsIgnoreCase("roundTrip")) {

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.cleartrip.android:id/bookBtn")));
			this.book_Button.click();

		}
	}

	@SuppressWarnings("unchecked")
	public void filter(String airLineName, boolean domestic) throws InterruptedException {

		if (common.elementPresent(driver, By.id("com.cleartrip.android:id/first_run_no_baggage_okay"), 15)) {

			this.firstRunNoBaggage.click();

		}

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='FILTER']")));

		this.domOwFilter.click();

		if (domestic) {

			this.seeMoreFilter.click();
		}

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Filter results']")));

		// common.scrollDown(driver,By.xpath("//android.widget.TextView[@text='Preferred
		// Airlines']"));
		common.scrollDown(driver, By.xpath("//android.widget.TextView[@text='"+airLineName+"']"));

		
		 * common.swipeDown(driver,"Preferred Airlines");
		 * 
		 * 
		 * common.swipeDown(driver, airLineName);
		 

		if (common.elementPresent(driver, By.xpath("//android.widget.TextView[@text='"+airLineName+"']"), 1)) {
			driver.findElement(By.xpath("//android.widget.TextView[@text='"+airLineName+"']")).click();
			
		}

		this.filterApplyButton.click();
	}

	
	 * private void scrollDown(By element) { Dimension dimensions =
	 * driver.manage().window().getSize(); int pressX =
	 * driver.manage().window().getSize().width / 2; System.out.println(pressX);
	 * 
	 * int bottomY = driver.manage().window().getSize().height * 4 / 5;
	 * System.out.println(bottomY);
	 * 
	 * int topY = driver.manage().window().getSize().height / 8;
	 * 
	 * for (int i = 0; i < dimensions.getHeight(); i++) {
	 * System.out.println(dimensions.getHeight());
	 * 
	 * TouchAction action = new TouchAction(driver); action.press(pressX,
	 * bottomY).waitAction(Duration.ofSeconds(5)).moveTo(pressX,
	 * topY).release().perform();
	 * 
	 * try { if (driver.findElement(element).isDisplayed()) break; } catch
	 * (Exception e) { System.out.println("element not visible"); }
	 * 
	 * }
	 * 
	 * }
	 

	public void confirmationPage() {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Booking is done']")));
		String confPrice = this.confPrice.getText();
		System.out.println("confPrice:" + confPrice);
		String tripId = this.tripId.getText();
		System.out.println("TripId:" + tripId);

		this.viewTripDetail.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Okay']")));
		this.okayTripDetail.click();

		System.out.println("from sector " + this.fromLegTripDetail.getText());
		System.out.println("to Sector " + this.toLegTripdetail.getText());

		common.scrollDown(driver, By.id("com.cleartrip.android:id/txtPaymentDetails"));

		System.out.println("BaseFare " + this.tripDetailBaseFare.getText());
		System.out.println("Tax" + this.tripDetailTax.getText());
		System.out.println("total fare" + this.tripDetailTotalPrice.getText());

	}

}
*/