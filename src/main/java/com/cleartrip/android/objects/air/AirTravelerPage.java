package com.cleartrip.android.objects.air;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.common.PageObject;

import io.appium.java_client.AppiumDriver;

public class AirTravelerPage extends PageObject {

	// ApppiumDriver driver;
	CommonUtil common = new CommonUtil();

	public AirTravelerPage(AppiumDriver driver) {
		super(driver);
	}

	@FindBy(xpath = "//android.widget.RadioButton[@text='Mr']")
	private WebElement adultTitle;

	@FindBy(id = "com.cleartrip.android:id/edt_first_name")
	private WebElement adultFirstName;

	@FindBy(id = "com.cleartrip.android:id/edt_last_name")
	private WebElement adultLastName;

	@FindBy(xpath = "//android.widget.RadioButton[@text='Mstr']")
	private WebElement childTitle;

	@FindBy(id = "com.cleartrip.android:id/edt_dob")
	private WebElement dob;

	@FindBy(xpath = "//android.widget.Button[@text='OK']")
	private WebElement okayButton;

	@FindBy(id = "com.cleartrip.android:id/nationality")
	private WebElement nationality;

	@FindBy(id = "com.cleartrip.android:id/save_traveller_info")
	private WebElement saveInfo;

	@FindBy(id = "com.cleartrip.android:id/edtTravellerMobile")
	private WebElement mobileNumber;

	@FindBy(id = "com.cleartrip.android:id/edtTravellerEmail")
	private WebElement travelerEmail;

	@FindBy(id = "com.cleartrip.android:id/btnTravellerBooking")
	private WebElement travelerContinueBooking;

	@FindBy(id = "com.cleartrip.android:id/txtUseGst")
	private WebElement gst;

	@FindBy(xpath = "//android.widget.TextView[@text='Add GST details']")
	private WebElement gstPage;

	@FindBy(id = "com.cleartrip.android:id/gstNumber")
	private WebElement gstNumber;

	@FindBy(id = "com.cleartrip.android:id/gstHolderName")
	private WebElement gstHolderName;

	@FindBy(id = "com.cleartrip.android:id/gstHolderAddress")
	private WebElement gstAddress;

	@FindBy(id = "com.cleartrip.android:id/gstDoneButton")
	private WebElement gstDoneButton;

	@FindBy(xpath = "//android.widget.EditText[@text='Visa Type']")
	private WebElement visaType;

	@FindBy(xpath = "//android.widget.CheckedTextView[@text='Business']")
	private WebElement Business;

	@FindBy(id = "com.cleartrip.android:id/edtPassportNo")
	private WebElement passPortNo;

	@FindBy(id = "com.cleartrip.android:id/edtPassproExpiryDate")
	private WebElement passPortExpiryDate;

	@FindBy(id = "com.cleartrip.android:id/edtPassportIssuingCountry")
	private WebElement issuingCountry;

	public void travellerPage(boolean domestic, boolean singlePax, boolean pick_traveler, boolean gds)
			throws Exception {

		if (domestic)

		{
			travlerInfo();
		}

		this.mobileNumber.click();

		//
		this.mobileNumber.sendKeys("1234567890");
		driver.hideKeyboard();
		this.travelerEmail.click();

		this.travelerEmail.sendKeys("test@gmail.com");
		driver.hideKeyboard();
		common.scrollDown(driver, By.id("com.cleartrip.android:id/btnTravellerBooking"));
		this.travelerContinueBooking.click();
	}

	@SuppressWarnings("unchecked")
	public void travlerInfo() throws Exception {

		List<WebElement> adultCount = driver
				.findElements(By.xpath("//android.widget.TextView[contains(@text,'Adult')]"));
		List<WebElement> childCount = driver
				.findElements(By.xpath("//android.widget.TextView[contains(@text,'Child')]"));
		List<WebElement> infantCount = driver
				.findElements(By.xpath("//android.widget.TextView[contains(@text,'Infant')]"));

		if (adultCount != null && !(adultCount.isEmpty())) {

			int i = 1;
			for (WebElement adult : adultCount) {

				driver.findElement(By.xpath("//android.widget.TextView[@text='Adult " + i + "']")).click();
				addTravelerName("Adult");
				if (common.elementPresent(driver, By.xpath("//android.widget.TextView[@text='Additional information']"),
						2)) {
					additionalInfo();
				}
				this.saveInfo.click();

				i++;
			}
		}
		if (childCount != null && !(childCount.isEmpty())) {

			int i = 1;
			for (WebElement child : childCount) {
				driver.findElement(By.xpath("//android.widget.TextView[@text='Child " + i + "']")).click();
				addTravelerName("child");
				if (common.elementPresent(driver, By.xpath("//android.widget.TextView[@text='Additional information']"),
						2)) {
					additionalInfo();
				}
				this.saveInfo.click();
				i++;

			}

		}

		if (infantCount != null && !(infantCount.isEmpty())) {

			int i = 1;
			for (WebElement infant : infantCount) {

				driver.findElement(By.xpath("//android.widget.TextView[@text='Infant " + i + "']")).click();
				addTravelerName("infant");
				if (common.elementPresent(driver, By.xpath("//android.widget.TextView[@text='Additional information']"),
						2)) {
					additionalInfo();
				}
				this.saveInfo.click();
				i++;
			}

		}

	}

	public void addTravelerName(String paxType) throws Exception {

		if (paxType.equalsIgnoreCase("Adult")) {

			this.adultTitle.click();
			this.adultFirstName.click();
			this.adultFirstName.sendKeys("test");
			this.adultLastName.click();
			this.adultLastName.sendKeys("test");
			driver.hideKeyboard();

		} else if (paxType.equalsIgnoreCase("child")) {
			this.childTitle.click();
			this.adultFirstName.click();
			this.adultFirstName.sendKeys("test");
			this.adultLastName.click();
			this.adultLastName.sendKeys("test");
			fillDOB("child");

		} else if (paxType.equalsIgnoreCase("infant")) {

			this.childTitle.click();
			this.adultFirstName.click();
			this.adultFirstName.sendKeys("test");
			this.adultLastName.click();
			this.adultLastName.sendKeys("test");
			fillDOB("infant");

		}

	}

	public void additionalInfo() throws InterruptedException {

		if (common.elementPresent(driver, By.xpath("//android.widget.EditText[@text='Visa Type']"), 3)) {
			this.visaType.click();
			this.Business.click();
		}
		if (common.elementPresent(driver, By.id("com.cleartrip.android:id/edtPassportNo"), 3)) {
			this.passPortNo.click();
			this.passPortNo.sendKeys("12345fgt");
			driver.hideKeyboard();
		}
		if (common.elementPresent(driver, By.id("com.cleartrip.android:id/edtPassproExpiryDate"), 3)) {
			this.passPortExpiryDate.click();
			int passPortExp = Integer.parseInt("2021");

			final String pattern = "DD-MM-yyyy";
			String dateIn = new SimpleDateFormat(pattern).format(new Date());
			int currYear = Integer.parseInt(dateIn.split("-")[2]);

			while (currYear != passPortExp) {
				common.shortSwipeUp(driver, 1.4);
				Thread.sleep(2000);
				currYear++;
			}
			if (common.elementPresent(driver, By.xpath("//android.widget.Button[@text='Done']"), 1)
					|| common.elementPresent(driver, By.xpath("//android.widget.Button[@text='OK']"), 1)) {
				this.okayButton.click();
			}

		}

		if (common.elementPresent(driver, By.id("com.cleartrip.android:id/edtPassportIssuingCountry"), 3)) {
			this.issuingCountry.click();
			this.issuingCountry.sendKeys("India");
		}
	}

	public void fillDOB(String type) throws Exception {

		if (common.elementPresent(driver, By.id("com.cleartrip.android:id/edt_dob"), 2)) {

			this.dob.click();

			int dobMonth = 0;
			int dobYear = 0;

			final String pattern = "dd-mm-yyyy";
			String dateIn = new SimpleDateFormat(pattern).format(new Date());

			if (type.equalsIgnoreCase("adult")) {
				dobMonth = Integer.parseInt("5");
				dobYear = Integer.parseInt("2000");

			} else if (type.equalsIgnoreCase("child")) {
				dobMonth = Integer.parseInt("5");
				dobYear = Integer.parseInt(dateIn.split("-")[2]) - 4;

			} else if (type.equalsIgnoreCase("infant")) {

				dobMonth = Integer.parseInt("5");
				dobYear = Integer.parseInt(dateIn.split("-")[2]) - 1;

			}

			int currentYear = Integer.parseInt(dateIn.split("-")[2]);

			while (currentYear != dobYear) {
				common.shortSwipeDown(driver, 1.4);
				Thread.sleep(2000);
				currentYear--;
			}
			if (common.elementPresent(driver, By.xpath("//android.widget.Button[@text='Done']"), 1)
					|| common.elementPresent(driver, By.xpath("//android.widget.Button[@text='OK']"), 1)) {
				this.okayButton.click();
			}

		}

	}

}
