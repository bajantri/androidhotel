package com.cleartrip.android.objects.air;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.common.PageObject;

import io.appium.java_client.AppiumDriver;

public class AirItineraryPage extends PageObject {

	public AirItineraryPage(AppiumDriver driver) {
		super(driver);
	}

	@FindBy(id = "com.cleartrip.android:id/done_button")
	private WebElement priceHikeOkay;

	@FindBy(id = "com.cleartrip.android:id/insuranceLyt")
	private WebElement insuranceCheckBox;

	@FindBy(id = "com.cleartrip.android:id/total_price")
	private WebElement totalPrice;

	@FindBy(id = "com.cleartrip.android:id/base_fare_text")
	private WebElement baseFare;

	@FindBy(id = "com.cleartrip.android:id/base_fare_text")
	private WebElement taxes_Fees;

	@FindBy(id = "com.cleartrip.android:id/insurance_txt")
	private WebElement insuranceBreakUp;

	@FindBy(xpath = "//android.widget.TextView[@text='Hide Fare Breakup']")
	private WebElement hideBreakUp;

	@FindBy(xpath = "//android.widget.TextView[@text='Show Fare Breakup']")
	private WebElement showBreakUp;

	@FindBy(id = "com.cleartrip.android:id/showFareBreakup")
	private WebElement showBreakUp1;

	@FindBy(id = "com.cleartrip.android:id/itryContinueBooking")
	private WebElement itineraryContinueBooking;

	@FindBy(id = "android:id/button1")
	private WebElement insuranceAdd;

	@FindBy(xpath = "//android.widget.Button[@text='NO THANKS']")
	private WebElement noInsurance;

	@FindBy(id = "com.cleartrip.android:id/continueBookingButton")
	private WebElement addOnContinueBooking;

	CommonUtil common = new CommonUtil();

	public void itnerary() throws InterruptedException {

		common.waitForElement(driver, By.xpath("//android.widget.TextView[@text='Review itinerary']"));
		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Review
		// itinerary']")));
		common.scrollDown(driver, By.id("com.cleartrip.android:id/itryContinueBooking"));
		this.itineraryContinueBooking.click();
 
		//driver.findElement(By.xpath("//android.widget.Button[@text='ADD']")).isDisplayed())
		
		if (common.elementPresent(driver, By.xpath("//android.widget.Button[@text='ADD']"), 3))
				 {
			driver.findElement(By.xpath("//android.widget.Button[@text='ADD']")).click();
		}

		if (common.elementPresent(driver, By.xpath("//android.widget.TextView[@text='Add-ons']"), 4)) {

			addonPage();
		}
	}

	public void addonPage() {

		// this.insuranceAdd.click();

		common.scrollDown(driver, By.id("com.cleartrip.android:id/continueBookingButton"));

		this.addOnContinueBooking.click();

	}

}
