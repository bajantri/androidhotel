package com.cleartrip.android.objects.air;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.common.PageObject;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.functions.ExpectedCondition;

public class AirHomePage extends PageObject {

	@FindBy(id = "com.android.packageinstaller:id/permission_allow_button")
	private WebElement location_popup;

	@FindBy(id = "com.cleartrip.android:id/original_travel_text")
	private WebElement switch_travel;

	@FindBy(xpath = "//android.widget.TextView[@text='Flights']")
	private WebElement flights;

	@FindBy(id = "com.cleartrip.android:id/fhf_oneway")
	private WebElement oneway;

	@FindBy(id = "com.cleartrip.android:id/fhf_roundtrip")
	private WebElement roundtrip;

	@FindBy(id = "com.cleartrip.android:id/fhf_multicity")
	private WebElement multicity;

	@FindBy(id = "com.cleartrip.android:id/flight_lyt_from")
	private WebElement from_city;

	@FindBy(id = "com.cleartrip.android:id/flight_lyt_to")
	private WebElement to_city;

	@FindBy(id = "com.cleartrip.android:id/filter_edittext")
	private WebElement sector_name;

	@FindBy(id = "com.cleartrip.android:id/localityName_in_setLocation")
	private WebElement autosuggest_name;

	@FindBy(id = "com.cleartrip.android:id/flight_lyt_depart_date")
	private WebElement depart_calendar;

	@FindBy(id = "com.cleartrip.android:id/txt_return_date")
	private WebElement return_calendar;

	@FindBy(xpath = "//android.widget.TextView[@text='19']")
	private WebElement depart_date;

	@FindBy(xpath = "//android.widget.TextView[@text='21']")
	private WebElement return_date;

	@FindBy(id = "com.cleartrip.android:id/fhf_flightLytTravellerPicker")
	private WebElement travler_Picker;

	@FindBy(xpath = "//android.widget.TextView[@text='You']")
	private WebElement you;

	@FindBy(id = "android:id/button1")
	private WebElement traveler_Done;

	@FindBy(id = "com.cleartrip.android:id/fhf_btn_search_flights")
	private WebElement flights_search;

	public AirHomePage(AppiumDriver driver) {

		super(driver);
	}

	CommonUtil common = new CommonUtil();
	WebDriverWait wait = new WebDriverWait(driver, 30);

	public void popUp() {

		WebDriverWait wait = new WebDriverWait(driver, 30);

		WebElement waiting = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("com.android.packageinstaller:id/permission_allow_button")));
		System.out.println(waiting.isDisplayed());
		if (waiting.isDisplayed()) {
			this.location_popup.click();
		} else {
			System.out.println("no location popUp");
		}
	}

	public void productSwitcher(String product) throws InterruptedException {

		if (product.equalsIgnoreCase("TRAVEL")) {

			WebElement waiting = wait.until(ExpectedConditions
					.presenceOfElementLocated(By.id("com.cleartrip.android:id/original_travel_text")));

			if (waiting.isDisplayed())

			{
				this.switch_travel.click();
			} else {
				System.out.println("not present");
			}
		}

	}

	public void subProduct(String subProduct) {

		wait.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='" + subProduct + "']")));

		if (subProduct.equalsIgnoreCase("Flights")) {

			this.flights.click();
		}

		else if (subProduct.equalsIgnoreCase("You")) {
			this.you.click();
		}

		else {
			System.out.println("none");
		}

	}

	public void oneWay(String trip_type) {

		if (trip_type.equalsIgnoreCase("oneWay")) {
			this.oneway.click();
		} else if (trip_type.equalsIgnoreCase("roundTrip")) {
			this.roundtrip.click();
		}
	}

	public void fromCity(String fromCity) throws Exception {
		this.from_city.click();
		Thread.sleep(1000);
		this.sector_name.sendKeys(fromCity);
		Thread.sleep(100);
		this.autosuggest_name.click();
	}

	public void toCity(String toCity) throws Exception {
		this.to_city.click();
		Thread.sleep(1000);
		this.sector_name.sendKeys(toCity);
		Thread.sleep(100);
		this.autosuggest_name.click();
	}

	public void departDate() throws Exception {

		this.depart_calendar.click();
		Thread.sleep(100);
		this.depart_date.click();
	}

	public void returnDate() throws Exception {

		this.return_calendar.click();
		Thread.sleep(100);
		this.return_date.click();

	}

	public String dateSelection(AppiumDriver driver) {

		LocalDate currentDate = LocalDate.now();
		String monthName = currentDate.getMonth().name();
		int year = currentDate.getYear();
		String month[] = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };

		for (int i = 0; i < month.length; i++) {
			if (monthName.equalsIgnoreCase("December")) {
				monthName = "February";
				break;
			} else if (monthName.equalsIgnoreCase("November")) {
				monthName = "January";
				break;
			} else if (monthName.equalsIgnoreCase(month[i])) {
				monthName = month[i + 1];
				break;
			}

		}
		System.out.println(monthName);
		return monthName + " " + year;
	}

	public void selectTraveller(String adultCount, String childCount, String infantCount) throws Exception {

		int aCount = Integer.parseInt(adultCount);
		int cCount = Integer.parseInt(childCount);
		int iCount = Integer.parseInt(infantCount);
		if ((aCount + cCount > 9) || (aCount < iCount)) {
			// System.out.println("Invalid Traveller Count : Returning");
			Reporter.log("Invalid Traveller Count : Returning");
			return;
		}
		this.travler_Picker.click();
		while (aCount > 1) {
			common.shortSwipe1(driver, 0.7);
			aCount--;
			Thread.sleep(2000);
		}
		while (cCount > 0) {
			common.shortSwipe1(driver, 1.0);
			cCount--;
		}
		while (iCount > 0) {
			common.shortSwipe1(driver, 1.3);
			iCount--;
		}

		this.traveler_Done.click();
	}

	public void searchFlights() throws InterruptedException {
		Thread.sleep(100);
		this.flights_search.click();
	}

	public void travelDate(String travelType, String onwardDate, String returnDate) throws Exception {

		Calendar calendar = Calendar.getInstance();
		System.out.println("calendar" + calendar);
		DateFormat simple = new SimpleDateFormat("DD MMM YYYY");
		String date = simple.format(calendar.getTime());
		System.out.println("date" + date);

	}

}
