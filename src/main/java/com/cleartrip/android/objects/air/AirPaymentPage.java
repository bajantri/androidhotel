package com.cleartrip.android.objects.air;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.common.PageObject;

import io.appium.java_client.AppiumDriver;

public class AirPaymentPage extends PageObject{

	public AirPaymentPage(AppiumDriver driver) {
		super(driver);
	}
	
	@FindBy(xpath="//android.widget.TextView[@text='Make payment']")
	private WebElement paymentPage;
	
	@FindBy(id="com.cleartrip.android:id/btnCreditNormal")
	private WebElement crediCard;
	
	@FindBy(id="com.cleartrip.android:id/btnNetbankingNormal")
	private WebElement netBanking;
	
	@FindBy (id="com.cleartrip.android:id/bankWallet")
	private WebElement thirdPartyWallet;
	
	@FindBy(id="com.cleartrip.android:id/card_number")
	private WebElement cardNumber;
	
	@FindBy(id="com.cleartrip.android:id/expiryDate")
	private WebElement expiryDate;
	
	@FindBy(id="com.cleartrip.android:id/monthLyt")
	private WebElement expiryMonth;
	
	@FindBy(id="com.cleartrip.android:id/yearLyt")
	private WebElement expiryYear;
	
	@FindBy(id="com.cleartrip.android:id/credit_cvv")
	private WebElement creditCvv;
	
	@FindBy (id="com.cleartrip.android:id/etxtCouponcode")
	private WebElement couponCode;
	
	@FindBy(id="com.cleartrip.android:id/check_savings")
	private WebElement checkSavings;
	
	@FindBy(id="com.cleartrip.android:id/txt_Money")
	private WebElement totalAmount;
	
	@FindBy (id="com.cleartrip.android:id/convFeeTextView")
	private WebElement pgFeeText;
	
	
	@FindBy(id="com.cleartrip.android:id/btnSubmitPayment")
	private WebElement paybutton;
	
	@FindBy(id="android:id/button1")
	private WebElement expiryOkay;
	
	
	CommonUtil common=new CommonUtil();
	
	public void paymentPage() throws Exception {
	//	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.cleartrip.android:id/btnCreditNormal")));
		common.waitForElement(driver,By.id("com.cleartrip.android:id/btnCreditNormal"));
		this.crediCard.click();
		this.cardNumber.click();
		this.cardNumber.sendKeys("5123456789012346");
		driver.hideKeyboard();
		this.expiryDate.click();
		common.expiryScroll(driver);
        this.expiryOkay.click();
		this.creditCvv.click();
		this.creditCvv.sendKeys("123");
		driver.hideKeyboard();
		common.scrollDown(driver,By.id("com.cleartrip.android:id/btnSubmitPayment"));
		this.paybutton.click();
	}
	
	
	
	
	

}
