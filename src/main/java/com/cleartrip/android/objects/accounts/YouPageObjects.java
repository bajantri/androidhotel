package com.cleartrip.android.objects.accounts;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.common.PageObject;

import io.appium.java_client.AppiumDriver;

public class YouPageObjects extends PageObject  {
	
	public YouPageObjects(AppiumDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@FindBy(id = "com.cleartrip.android:id/signin_btn")
	private WebElement signInOrRegisterButton;
	
	@FindBy(xpath="//android.widget.TextView[@text='Account']")
	WebElement accountSection;
	
	@FindBy(xpath="//android.widget.TextView[@text='Referrals']")
	WebElement referralsLink;
	
	@FindBy(xpath="//android.widget.TextView[@text='Expressway']")
	WebElement expresswayLink;
	
	@FindBy(xpath="//android.widget.TextView[@text='Wallets']")
	WebElement walletLink;
	
	@FindBy(xpath="//android.widget.TextView[@text='More']")
    WebElement moreSection;
	
	@FindBy(xpath="//android.widget.TextView[@text='Country']")
	WebElement settingsLink;
	
	
	@FindBy(xpath="//android.widget.TextView[@text='Rate & Feedback']")
	WebElement rateAndFeedbackLink;
	
	@FindBy(xpath="//android.widget.TextView[@text='Support']")
	WebElement supportLink;
	
	@FindBy(xpath="//android.widget.TextView[@text='Terms of use']")
	WebElement termsOfUseLink;
	
	@FindBy(xpath="//android.widget.TextView[@text='Privacy policy']")
	WebElement privacyPolicyLink;
	
	@FindBy(xpath="//android.widget.TextView[@text='You']")
	WebElement youTab;
	
	@FindBy(id="com.cleartrip.android:id/countryLayout")
	WebElement countryLayout;
	
	@FindBy(id="com.cleartrip.android:id/cli_radioButton")
	List<WebElement> countryLayoutInNewYouTab;
	
	public List<WebElement> getCountryLayoutInNewYouTab() {
		return countryLayoutInNewYouTab;
	}
	
	

	public WebElement getCountryLayout() {
		return countryLayout;
	}

	public WebElement getYouTab() {
		return youTab;
	}

	public WebElement getSignInOrRegisterButton() {
		return signInOrRegisterButton;
	}

	public WebElement getAccountSection() {
		return accountSection;
	}

	public WebElement getReferralsLink() {
		return referralsLink;
	}

	public WebElement getExpresswayLink() {
		return expresswayLink;
	}

	public WebElement getWalletLink() {
		return walletLink;
	}

	public WebElement getMoreSection() {
		return moreSection;
	}

	public WebElement getSettingsLink() {
		return settingsLink;
	}

	
	public WebElement getRateAndFeedbackLink() {
		return rateAndFeedbackLink;
	}
	
	public WebElement getSupportLink() {
		return supportLink;
	}


	public WebElement getTermsOfUseLink() {
		return termsOfUseLink;
	}

	public WebElement getPrivacyPolicyLink() {
		return privacyPolicyLink;
	}
	
	
	public void signInOrRegisterButtonClick(){
		getSignInOrRegisterButton().click();		
	}
	
	public void referralLinkClick(){
		getReferralsLink().click();
	}
	
	public void expresswayLinkClick(){
		getExpresswayLink().click();
	}
	
	public void walletLinkClick(){
		getWalletLink().click();
	}
	
	public void sellingsLinkClick(){
		getSettingsLink().click();
	}
	
	public void supportLinkClick(){
		getSupportLink().click();
	}
	
	public void rateAndFeedbackLinkClick(){
		getRateAndFeedbackLink();
	}
	
	public void termsOfUseLinkClick(){
		getTermsOfUseLink().click();
	}
	
	public void privacyPolicyLinkClick(){
		getPrivacyPolicyLink().click();
	}
	
	public void selectCountry(AppiumDriver driver,String country) throws InterruptedException {
		CommonUtil util = new CommonUtil();
		util.waitForElement(driver, By.xpath("//android.widget.TextView[@text='You']"));
		getYouTab().click();
		util.swipeDown(driver, "Country");
		getSettingsLink().click();
		//getCountryLayout().click();
		getCountryLayoutInNewYouTab().get(9).click();
		//driver.findElement(By.xpath("//android.widget.RadioButton[@text='"+country+"'"+"]")).click();
		//driver.navigate().back();
		
			
	}

	
}













