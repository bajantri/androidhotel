package com.cleartrip.android.objects.accounts;

import java.io.IOException;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.cleartrip.android.common.CommonUtil;
import com.cleartrip.android.common.Main;
import com.cleartrip.android.common.PropertyUtil;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.HidesKeyboard;

public class SignInAndRegisterPageObjects extends YouPageObjects {

	public SignInAndRegisterPageObjects(AppiumDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@FindBy(xpath = "//android.widget.TextView[@text='SIGN IN']")
	WebElement signInTab;

	@FindBy(xpath = "//android.widget.TextView[@text='REGISTER']")
	WebElement registerTab;

	@FindBy(id = "com.cleartrip.android:id/fb_dup_btn")
	WebElement faceBookSignInButton;

	@FindBy(id = "com.cleartrip.android:id/username_field")
	WebElement emailAddressInputText;

	@FindBy(id = "com.cleartrip.android:id/password_field")
	WebElement passwordInputText;

	@FindBy(id = "com.cleartrip.android:id/forgot_password_button")
	WebElement forgotPasswordLink;

	@FindBy(id = "com.cleartrip.android:id/signin_button")
	WebElement signInButton;

	@FindBy(id = "com.cleartrip.android:id/mrRb")
	WebElement titleSelection;

	@FindBy(id = "com.cleartrip.android:id/signup_fname")
	WebElement signUpFirstName;

	@FindBy(id = "com.cleartrip.android:id/signup_lname")
	WebElement signUpLastName;

	@FindBy(id = "com.cleartrip.android:id/signup_email")
	WebElement signUpEmailAddress;

	@FindBy(id = "com.cleartrip.android:id/signup_phNum")
	WebElement signUpMobileNumber;

	@FindBy(id = "com.cleartrip.android:id/signup_pwd")
	WebElement signUpPassword;

	@FindBy(id = "com.cleartrip.android:id/cb_promotional_mails")
	WebElement subscribeToOffersCheckBox;

	@FindBy(id = "com.cleartrip.android:id/signup_register_button")
	WebElement signUpRegisterButton;

	public WebElement getSignInTab() {
		return signInTab;
	}

	public WebElement getRegisterTab() {
		return registerTab;
	}

	public WebElement getFaceBookSignInButton() {
		return faceBookSignInButton;
	}

	public WebElement getEmailAddressInputText() {
		return emailAddressInputText;
	}

	public WebElement getPasswordInputText() {
		return passwordInputText;
	}

	public WebElement getForgotPasswordLink() {
		return forgotPasswordLink;
	}

	public WebElement getSignInButton() {
		return signInButton;
	}

	public WebElement getTitleSelection() {
		return titleSelection;
	}

	public WebElement getSignUpFirstName() {
		return signUpFirstName;
	}

	public WebElement getSignUpLastName() {
		return signUpLastName;
	}

	public WebElement getSignUpEmailAddress() {
		return signUpEmailAddress;
	}

	public WebElement getSignUpMobileNumber() {
		return signUpMobileNumber;
	}

	public WebElement getSignUpPassword() {
		return signUpPassword;
	}

	public WebElement getSubscribeToOffersCheckBox() {
		return subscribeToOffersCheckBox;
	}

	public WebElement getSignUpRegisterButton() {
		return signUpRegisterButton;
	}

	public void signInTabClick() {
		getSignInTab().click();
	}

	public void emailAddressClickAndInput(String emailAddress) {
		getEmailAddressInputText().clear();
		getEmailAddressInputText().sendKeys(emailAddress);
	}

	public void passwordInput(String password) {
		getPasswordInputText().clear();
		getPasswordInputText().sendKeys(password);
	}

	public void signInButtonClick() {
		getSignInButton().click();
	}

	public void signIn(AppiumDriver driver) throws IOException, InterruptedException {
		HashMap<String, Object> params = new HashMap<>();
		params = signInCredentials();
		signing(driver, params);
		Thread.sleep(2000);

	}

	public void loginSpecificToPaymentMode(AppiumDriver driver, String productName, String paymentMode)
			throws IOException {
		HashMap<String, Object> params = new HashMap<>();
		params = signInCredentialsSpecificToPaymentMode(productName, paymentMode);
		signing(driver, params);
	}

	public void signing(AppiumDriver driver, HashMap<String, Object> signInParams) {
		CommonUtil util = new CommonUtil();
		util.waitForElement(driver, By.id("com.cleartrip.android:id/achf_user_message"));
		getSignInOrRegisterButton().click();
		util.waitForElement(driver, By.id("com.cleartrip.android:id/fb_dup_btn"));
		driver.hideKeyboard();
		boolean elementPresent = util.waitForElement(driver, By.id("com.cleartrip.android:id/signin_button"));
		if (elementPresent) {
			// signInTabClick();
			emailAddressClickAndInput(signInParams.get("username").toString());
			driver.hideKeyboard();
			passwordInput(signInParams.get("password").toString());
			driver.hideKeyboard();
			signInButtonClick();
			util.waitForElementAsString(driver, "Bookings");

		} else {
			System.out.println("element not present");
		}
	}

	public HashMap<String, Object> signInCredentials() throws IOException {
		HashMap<String, Object> params = new HashMap<>();
		if (Main.getEnv("env").equalsIgnoreCase("qa2")) {
			params.put("username", PropertyUtil.getCommonData("qausername"));
			params.put("password", PropertyUtil.getCommonData("qapassword"));
		} else if (Main.getEnv("env").equalsIgnoreCase("hf")) {
			params.put("username", PropertyUtil.getCommonData("hfusername"));
			params.put("password", PropertyUtil.getCommonData("hfpassword"));
		} else if (Main.getEnv("env").equalsIgnoreCase("prodorbeta")) {
			params.put("username", PropertyUtil.getCommonData("prodorbetausername"));
			params.put("password", PropertyUtil.getCommonData("prodorbetapassword"));
		}
		return params;

	}

	/*
	 * Here we are not checking environment as qa2, dev, hf and production since
	 * we will be running all the cases in QA2 only.
	 */
	public HashMap<String, Object> signInCredentialsSpecificToPaymentMode(String productName, String paymentMode)
			throws IOException {
		HashMap<String, Object> params = new HashMap<>();
		if (productName.equalsIgnoreCase("air")) {
			if (paymentMode.equalsIgnoreCase("partialwallet")) {
				params.put("username", PropertyUtil.getCommonData("airqapartialwalletusername"));
				params.put("password", PropertyUtil.getCommonData("airqapartialwalletpassword"));
			} else if (paymentMode.equalsIgnoreCase("fullwallet")) {
				params.put("username", PropertyUtil.getCommonData("airqafullwalletusername"));
				params.put("password", PropertyUtil.getCommonData("airqafullwalletpassword"));
			} else if (paymentMode.equalsIgnoreCase("storedcard")) {
				params.put("username", PropertyUtil.getCommonData("airstoredcardusername"));
				params.put("password", PropertyUtil.getCommonData("airstoredcardpassword"));
			} else {
				params.put("username", PropertyUtil.getCommonData("qausername"));
				params.put("password", PropertyUtil.getCommonData("qapassword"));
			}
		} else if (productName.equalsIgnoreCase("hotels")) {
			if (paymentMode.equalsIgnoreCase("partialwallet")) {
				params.put("username", PropertyUtil.getCommonData("hotelqapartialwalletusername"));
				params.put("password", PropertyUtil.getCommonData("hotelqapartialwalletpassword"));
			} else if (paymentMode.equalsIgnoreCase("fullwallet")) {
				params.put("username", PropertyUtil.getCommonData("hotelqafullwalletusername"));
				params.put("password", PropertyUtil.getCommonData("hotelqafullwalletpassword"));
			} else if (paymentMode.equalsIgnoreCase("storedcard")) {
				params.put("username", PropertyUtil.getCommonData("hotelstoredcardusername"));
				params.put("password", PropertyUtil.getCommonData("hotelstoredcardpassword"));
			} else {
				params.put("username", PropertyUtil.getCommonData("qausername"));
				params.put("password", PropertyUtil.getCommonData("qapassword"));
			}
		} else if (productName.equalsIgnoreCase("local")) {
			if (paymentMode.equalsIgnoreCase("partialwallet")) {
				params.put("username", PropertyUtil.getCommonData("localqapartialwalletusername"));
				params.put("password", PropertyUtil.getCommonData("localqapartialwalletpassword"));
			} else if (paymentMode.equalsIgnoreCase("fullwallet")) {
				params.put("username", PropertyUtil.getCommonData("localqafullwalletusername"));
				params.put("password", PropertyUtil.getCommonData("localqafullwalletpassword"));
			} else if (paymentMode.equalsIgnoreCase("storedcard")) {
				params.put("username", PropertyUtil.getCommonData("localstoredcardusername"));
				params.put("password", PropertyUtil.getCommonData("localstoredcardpassword"));
			} else {
				params.put("username", PropertyUtil.getCommonData("qausername"));
				params.put("password", PropertyUtil.getCommonData("qapassword"));
			}
		}
		return params;

	}

	public void registerTabClick() {
		getRegisterTab().click();
	}

	public void selectTitle() {
		getTitleSelection().click();
	}

	public void firstNameSignUpInput(String firstName) {
		getSignUpFirstName().clear();
		getSignUpFirstName().sendKeys(firstName);
	}

	public void lastNameSignUpInput(String lastName) {
		getSignUpLastName().clear();
		getSignUpLastName().sendKeys(lastName);
	}

	public void emailAddressSignUpInput(String emailAddress) {
		getSignUpEmailAddress().clear();
		getSignUpEmailAddress().sendKeys(emailAddress);
	}

	public void mobileNumberSignUpInput(String mobileNumber) {
		getSignUpMobileNumber().clear();
		getSignUpMobileNumber().sendKeys(mobileNumber);
	}

	public void passwordSignUpInput(String password) {
		getSignUpPassword().clear();
		getSignUpPassword().sendKeys(password);
	}

	public void subscribeOffer() {
		getSubscribeToOffersCheckBox().click();
	}

	public void registerButtonClick() {
		getSignUpRegisterButton().click();
	}

	public void register(AppiumDriver driver, HashMap<String, String> registerParams) {
		getSignInOrRegisterButton().click();
		CommonUtil util = new CommonUtil();
		boolean elementPresent = util.waitForElementAsString(driver, "REGISTER");
		if (elementPresent) {
			selectTitle();
			firstNameSignUpInput(registerParams.get("firstName"));
			lastNameSignUpInput(registerParams.get("lastName"));
			emailAddressSignUpInput(registerParams.get("emailAddress"));
			mobileNumberSignUpInput(registerParams.get("mobileNumber"));
			passwordSignUpInput(registerParams.get("password"));
			subscribeOffer();
			registerButtonClick();
		}
	}
}
